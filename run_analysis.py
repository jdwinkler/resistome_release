import AminoAcidAnalysis
import DescriptiveStatistics
import GenomeAnalysis
import ProteinAnalysis
import FluxAnalysis
import InteractionAnalysis
import ModificationVisualization
import recommendation_system


def main():

    AminoAcidAnalysis.run_analysis()
    DescriptiveStatistics.run_analysis()
    GenomeAnalysis.run_analysis()
    ProteinAnalysis.run_analysis()
    FluxAnalysis.run_analysis()
    ModificationVisualization.generate_ecoli_config(output_prefix='gene_')

if __name__ == '__main__':
    main()