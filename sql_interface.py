from collections import defaultdict
import database_constants
import psycopg2
import psycopg2.extras

species = database_constants.SPECIES_LIST


class ConnectionManager:

    """
    
    Class that handles details related to connecting to the Resistome database.
    
    """

    def __init__(self, username=None, password=None, db_name='resistome'):

        self.cursor, self.connection = self.get_database_cursor(user_name=username,
                                                                password=password,
                                                                database_name=db_name)

    def get_database_cursor(self,
                            user_name,
                            password,
                            database_name):
        """
    
        Returns a connection and DictCursor object for use in querying/DB modification.
    
        Assumes a database called resistome exists, although the db credentials and name
        can be changed in inputs/db_credentials/credentials.txt.
    
        :return: 
        """

        connection = psycopg2.connect("dbname='%s' user='%s' host='localhost' password='%s'"
                                      % (database_name, user_name, password))
        cursor = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)

        return cursor, connection


def get_genetic_code(query_species='mg1655'):
    """

    Builds two objects representing the E. coli genetic code:

    codon to AA mapping (dict (codon: AA), all str)
    AA to codon reverse mapping
    
    Query species currently does nothing, but in the event that the resistome is used to host other organisms with
    different genetic codes it will be used to return a particular species' code.

    :param query_species:
    :return: 
    """

    cursor.execute('select codon, aa from public.genetic_code')

    code = {}
    aa_to_codon = defaultdict(list)

    for result in cursor:
        # codon is a string, not a set of codons
        code[result['codon']] = result['aa']
        aa_to_codon[result['aa']].append(result['codon'])

    return code, aa_to_codon


def get_go_terms(terms_to_extract):

    """
    
    Returns a dict of go_term (GO:XXXXXXX) : name of term.
    
    :param terms_to_extract: 
    :return: 
    """

    cursor.execute('select go_term, name from public.go_table where go_term = ANY(%s)',
                   (terms_to_extract,))

    go_tag_name = dict()
    for result in cursor:
        go_tag_name[result['go_term']] = result['name']

    return go_tag_name


def get_go_adjacency_list():
    """

    Returns an adjaency list (parent, child) edges for the GO graph encoded in the database.

    :return: 
    """

    cursor.execute('select parent, child from public.go_adj_list')

    output = []

    for result in cursor:
        output.append((result['parent'], result['child']))

    return output


def get_gene_label_relationships(type_of_feature):

    """
    
    Builds accession : identifier list; can request accession-go, accession-metabolite, accession-uniprot relationships.
    
    :param type_of_feature: 
    :return: 
    """

    if type_of_feature not in {'go', 'metabolite', 'uniprot'}:
        raise AssertionError('Unknown type_of_feature to build gene-value dict from: %s' % type_of_feature)

    if type_of_feature == 'go':
        type_of_feature = 'go_terms'
        field = 'go_term'
    elif type_of_feature == 'uniprot':
        field = 'region'
    else:
        type_of_feature = 'metabolomics'
        field = 'metabolite'

    query = []
    for strain in species:
        query.append('select * from %s.%s ' % (strain, type_of_feature))

    final_query = 'UNION ALL '.join(query)
    cursor.execute(final_query)
    output_tuples = []

    for result in cursor:
        output_tuples.append((result['accession'], result[field]))

    return output_tuples


def get_distinct_entities(entity):

    """
    
    Returns the list of unique terms (entity = 'gene', 'go', or 'metabolite') and their count.
    
    :param entity: 
    :return: 
    """

    if entity == 'gene':
        cursor.execute('select distinct on (name) name from resistome.mutations')
    elif entity == 'go':
        cursor.execute('select distinct go_term from public.go_table')
    elif entity == 'metabolite':
        cursor.execute('select distinct ion_id from public.ms_ions')
    else:
        raise AssertionError('Unknown entity type requested: %s' % entity)

    records = cursor.fetchall()

    return records, len(records)


def get_uniprot_location_data(strain, accession, protein_position):

    """
    
    
    :param strain: 
    :param accession: 
    :param protein_position: 
    :return: 
    """

    results = []

    cursor.execute('select region, start, stop from ' + strain + '.uniprot where '
                                                                 '(accession = %s AND start <= %s AND stop >= %s)',
                   (accession, protein_position, protein_position))

    results.extend(cursor.fetchall())

    return results


def get_aa_stability_position_value(gene, target_strain, position, wt_aa, mut_aa, method, skip_errors=False):

    """
    
    :param gene: 
    :param target_strain: 
    :param position: 
    :param wt_aa: 
    :param mut_aa: 
    :param method: 
    :param skip_errors: 
    :return: 
    """

    if target_strain.lower() not in species:
        raise AssertionError('Unknown species requested for protein stability: %s' % target_strain)

    if skip_errors:
        # less stringent, will succeed unless gene is missing entirely
        cursor.execute('select score from ' + target_strain + '.protein_stability '
                                                              'where accession = %s '
                                                              'AND position = %s '
                                                              'AND mutant_aa = %s '
                                                              'AND method = %s',
                       (gene, position, mut_aa, method))
    else:
        # forces query to only match if WT AA = that in sequence record
        cursor.execute('select score from ' + target_strain + '.protein_stability '
                                                              'where accession = %s '
                                                              'AND position = %s '
                                                              'AND wt_aa = %s '
                                                              'AND mutant_aa = %s '
                                                              'AND method = %s',
                       (gene, position, wt_aa, mut_aa, method))

    results = cursor.fetchone()

    return results


def extract_protein_stability_data(method, query_species='mg1655'):
    """

    Extracts all gene stability data for all species and returns it as a list of dicts.

    accession
    position
    wt_aa
    mut_aa
    score

    :param method: 
    :param query_species
    :return: 
    """

    if method not in {'SNAP2', 'INPS'}:
        raise AssertionError('Unknown method requested: %s' % method)

    if query_species not in species:
        raise AssertionError('Unknown species requested: %s' % species)

    final_query = 'select * from %s.%s' % (query_species, 'protein_stability') + ' where method = upper(%s) '

    cursor.execute(final_query, (method,))

    results = cursor.fetchall()

    if results is None:
        raise ValueError('No results obtained')
    else:
        return results


def get_gene_identifiers(query_species='mg1655'):

    """
    
    Returns distinct gene names for the species requested.
    
    :param query_species: 
    :return: 
    """

    if query_species not in species:
        raise AssertionError('Unknown species gene set requested: %s' % query_species)

    cursor.execute('select distinct UPPER(accession) as accession from ' + query_species + '.genes')

    results = cursor.fetchall()

    return [x['accession'] for x in results]


def get_gene_common_names(gene_set, query_species='mg1655'):
    """

    Returns the commonly used name for the requested genes.

    :param query_species: 
    :return: 
    """

    if query_species not in species:
        raise AssertionError('Unknown species gene set requested: %s' % query_species)

    cursor.execute('select upper(accession) as accession, name  from ' + query_species +
                   '.genes where accession = ANY(%s)',
                   (list(gene_set),))

    results = cursor.fetchall()

    output_dict = dict()

    for result in results:
        output_dict[result['accession']] = result['name']

    return output_dict


def get_genome_sequence(query_species='mg1655'):
    """

    Returns the genome sequence for the requested species. Does not handle multiple chromosomes.

    :param query_species: 
    :return: 
    """

    if query_species not in species:
        raise AssertionError('Unknown species genome requested: %s' % query_species)

    cursor.execute('select sequence from ' + query_species + '.genome')

    return cursor.fetchone()['sequence']


def get_gene_location_data(query_species='mg1655'):
    """

    Returns a dict (accession : dict (start, stop: int) representing the position of all genes in the requested genome.

    :param query_species:
    :return: 
    """

    genome_data = {}

    if query_species not in species:
        raise AssertionError('Unknown species gene data requested: %s' % query_species)

    cursor.execute('select start, stop, accession from ' + query_species + '.genes')

    for result in cursor:
        genome_data[result['accession']] = {'start': result['start'],
                                            'stop': result['stop']}

    return genome_data


def get_gene_coding_sequence(accession, query_species='mg1655'):

    """
    
    Returns the polypeptide sequence associated with accession. None is returned for sequences without translated
    counterparts.
    
    :param accession: 
    :param query_species: 
    :return: 
    """

    if query_species not in species:
        raise AssertionError('Unknown species coding sequence requested: %s' % query_species)

    cursor.execute('select aa_seq from ' + query_species + '.aa_sequences where accession = %s', (accession,))

    # don't assume any particular form of the cursor (dict, normal, etc...)
    # let the caller handle
    # quits once any result is found, since gene ids are unique
    results = cursor.fetchone()

    if results is None:
        return None
    else:
        return results['aa_seq']


def filter_genes_by_feature(genes, features, type_of_feature, query_species='mg1655'):

    features = list(features)
    genes = list(genes)

    if type_of_feature == 'go':
        cursor.execute('select accession from ' + query_species + '.go_terms '
                       'where go_term = ANY(%s) and accession = ANY(%s)', (features, genes))
    elif type_of_feature == 'metabolite':
        cursor.execute('select accession from ' + query_species + '.metabolomics '
                       'where metabolite = ANY(%s) and accession = ANY(%s)', (features, genes))
    elif type_of_feature == 'gene':
        raise AssertionError('Cannot filter gene list by gene features!')
    else:
        raise AssertionError('Unknown feature type: %s' % features)

    return set([x['accession'] for x in cursor])


def get_gene_physical_interactions(genes, query_species='mg1655'):

    return get_gene_interactions(genes, 'ecolinet_v1', query_species=query_species)


def get_gene_regulons(genes, query_species='mg1655'):

    return get_gene_interactions(genes, 'regulondb', query_species=query_species)


def get_gene_interactions(genes, method, query_species='mg1655'):

    genes = list(genes)

    cursor.execute('select '
                   'regulator as regulator, '
                   'array_agg(target) as regulon, '
                   'array_agg(direction) as regulation_direction '
                   'from ' + query_species + '.interactions '
                   'where regulator = ANY(%s) and db_source = %s '
                   'group by regulator', (genes, method))

    output_dict = dict()
    for result in cursor:
        output_dict[result['regulator']] = [(gene, dir) for gene, dir in zip(result['regulon'],
                                                                             result['regulation_direction'])]

    return output_dict


def get_mutant_genotypes():

    """
    
    Aggregate resistome data back into genotypes.
    
    :return: 
    """

    cursor.execute('select '
                   'resistome.mutants.mutant_id, '
                   'resistome.mutants.paper_id, '
                   'resistome.mutants.strain,'
                   'array_agg(resistome.phenotypes.phenotype) as phenotypes,'
                   'array_agg(resistome.phenotypes.phenotype_class) as pclasses,'
                   'array_agg(resistome.phenotypes.phenotype_type) as types '
                   'from resistome.mutants '
                   'left join resistome.phenotypes on (resistome.phenotypes.mutant_id = resistome.mutants.mutant_id) '
                   'group by resistome.mutants.mutant_id')

    phenotype_results = cursor.fetchall()

    cursor.execute('select '
                   'resistome.mutations.mutant_id, '
                   'array_agg(resistome.mutations.gene_id) as gene_ids, '
                   'array_agg(resistome.mutations.name) as species_names, '
                   'array_agg(resistome.gene_standardization.mg1655_accession) as mg1655_names, '
                   'array_agg(resistome.annotations.mutation_type) as mutations, '
                   'array_agg(resistome.annotations.annotation) as annotations '
                   'from resistome.mutations '
                   'left join resistome.annotations on (resistome.annotations.gene_id = resistome.mutations.gene_id) '
                   'inner join resistome.gene_standardization on (resistome.gene_standardization.species_accession = resistome.mutations.name '
                   '                                              AND upper(resistome.gene_standardization.strain) = upper(resistome.mutations.strain)) '
                   'group by resistome.mutations.mutant_id')

    genotype_results = cursor.fetchall()

    genotype_dicts = defaultdict(dict)

    for result in genotype_results:

        genotype_dicts[result['mutant_id']]['mutant_id'] = result['mutant_id']
        genotype_dicts[result['mutant_id']]['gene_ids'] = result['gene_ids']
        genotype_dicts[result['mutant_id']]['species_names'] = set(result['species_names'])
        genotype_dicts[result['mutant_id']]['mg1655_names'] = set(result['mg1655_names'])
        genotype_dicts[result['mutant_id']]['paired_species_names'] = result['species_names']
        genotype_dicts[result['mutant_id']]['paired_mg1655_names'] = result['mg1655_names']

        if None in result['mutations']:
            genotype_dicts[result['mutant_id']]['mutations'] = []
        else:
            genotype_dicts[result['mutant_id']]['mutations'] = result['mutations']

        if None in result['annotations']:
            genotype_dicts[result['mutant_id']]['annotations'] = []
        else:
            genotype_dicts[result['mutant_id']]['annotations'] = result['annotations']

    for result in phenotype_results:

        if 'mutant_id' not in genotype_dicts[result['mutant_id']]:
            genotype_dicts[result['mutant_id']]['mutant_id'] = result['mutant_id']

            genotype_dicts[result['mutant_id']]['species_names'] = []
            genotype_dicts[result['mutant_id']]['mg1655_names'] = []
            genotype_dicts[result['mutant_id']]['paired_species_names'] = []
            genotype_dicts[result['mutant_id']]['paired_mg1655_names'] = []
            genotype_dicts[result['mutant_id']]['mutations'] = []
            genotype_dicts[result['mutant_id']]['annotations'] = []
            genotype_dicts[result['mutant_id']]['gene_ids'] = []

        genotype_dicts[result['mutant_id']]['paper_id'] = result['paper_id']
        genotype_dicts[result['mutant_id']]['strain'] = result['strain']
        genotype_dicts[result['mutant_id']]['phenotypes'] = result['phenotypes']
        genotype_dicts[result['mutant_id']]['pclasses'] = result['pclasses']
        genotype_dicts[result['mutant_id']]['types'] = result['types']

    return genotype_dicts.values()


def get_gene_gene_graph(source='genetic', edge_count_threshold=50):

    """

    Build interaction graph to connect genes that are either mutated in the same strain or differentially
    expressed in the same strain.

    :param cursor: database cursor for resistome
    :param source: str, either 'genetic' or 'expression' to indicate what type of interaction graph is deisred
    :param edge_count_threshold: int, minimum count of edges to include
    :return: networkx interaction graph G, set of nodes included, dict (str: int) of node counts
    """

    import networkx
    import itertools

    if source == 'genetic':

        cursor.execute('select '
                       'array_agg(resistome.gene_standardization.mg1655_accession) as names,'
                       'array_agg(resistome.annotations.mutation_type) as mtypes '
                       'from resistome.mutations '
                       'inner join resistome.annotations on '
                       '(resistome.mutations.gene_id = resistome.annotations.gene_id) '
                       'inner join resistome.gene_standardization on '
                       '(resistome.mutations.name = resistome.gene_standardization.species_accession) '
                       'GROUP BY resistome.mutations.mutant_id')

    elif source == 'expression':

        cursor.execute('select '
                       'array_agg(resistome.gene_standardization.mg1655_accession) as names,'
                       'array_agg(resistome.expressions.fold_change) as mtypes '
                       'from resistome.expressions '
                       'inner join resistome.gene_standardization on '
                       '(resistome.expressions.name = resistome.gene_standardization.species_accession) '
                       'GROUP BY resistome.expressions.mutant_id')

    else:
        raise AssertionError('Unknown source data for graph generation')

    node_mutations = defaultdict(set)
    node_counter = defaultdict(int)
    edge_weights = defaultdict(int)
    edge_set = set()
    node_set = set()

    design_count = 0

    for result in cursor:

        names = result['names']
        m_types = result['mtypes']

        nodes_to_process = []

        for name, mutation in zip(names, m_types):

            if source == 'genetic':

                node_counter[name] += 1
                node_mutations[name].add(mutation)
                node_set.add(name)
                nodes_to_process.append(name)

            elif source == 'expression':

                # filter for weakly differentially expressed genes
                if mutation is None or abs(mutation) < 5.0:
                    continue

                node_counter[name] += 1
                node_mutations[name].add(mutation)
                node_set.add(name)
                nodes_to_process.append(name)

        gene_gene_product = itertools.product(nodes_to_process, nodes_to_process)

        for (g1, g2) in gene_gene_product:

            if g1 == g2:
                continue

            edge_set.add((g1, g2))
            edge_set.add((g2, g1))

            edge_weights[(g1, g2)] += 1
            edge_weights[(g2, g2)] += 1

        design_count += 1

    added = set()

    G = networkx.Graph(number_of_designs=design_count)
    for edge in edge_set:

        if edge_weights[edge] > edge_count_threshold:
            G.add_edge(edge[0], edge[1], weight=edge_weights[edge])
            added.add(edge[0])

    for node in node_set - added:
        G.add_node(node)

    networkx.set_node_attributes(G, 'mutations', dict(node_mutations))

    return G, node_set, node_counter


def get_mutant_genotypes_as_features(type_of_feature):

    if type_of_feature not in {'gene', 'go', 'metabolite'}:
        raise AssertionError('Unknown feature type requested: %s' % type_of_feature)

    records = get_mutant_genotypes()
    output_results = []
    for result in records:

        genes = result['mg1655_names']

        if type_of_feature == 'go' or type_of_feature == 'metabolite':
            converted_features = convert_genes_to_features(list(genes),
                                                               type_of_feature,
                                                               query_species=result['strain'])
        else:
            converted_features = genes

        result[type_of_feature] = set(converted_features)
        output_results.append(result)

    return output_results


def get_mutation_location_data(location_data, debug_flag=False):

    """
    Extracts mutational data from the resistome and converts location data to absolute positioning for use by
    circos.

    :param cursor: database cursor to access resistome database 
    :param location_data: dict of (str: (int, int) where str is a gene accession, (int1 = start, int2=end)
    :return: iterable of tuples (start, end, resistome mutation type)
    """

    cursor.execute('select resistome.mutations.name, '
                   'array_agg(resistome.annotations.annotation) as annotations,'
                   'array_agg(resistome.annotations.mutation_type) as mutation_types '
                   'from resistome.mutations '
                   'inner join resistome.annotations on (resistome.mutations.gene_id = resistome.annotations.gene_id) '
                   'group by resistome.mutations.gene_id')

    if debug_flag:
        return cursor.fetchall()

    output_tuples = []

    for result in cursor:

        name = result['name']
        mutation_types = result['mutation_types']
        annotations = result['annotations']

        location_info = location_data.get(name, None)

        # some mutations are not specifically located in the target papers
        if location_info is None:
            continue

        if len(location_data) > 2:
            start = location_info[1]
            stop = location_info[2]
        else:
            start = location_info[0]
            stop = location_info[1]

        for mutation_type, annotation in zip(mutation_types, annotations):

            if mutation_type in database_constants.DEL_mutations:

                output_tuples.append((start,
                                      stop,
                                      mutation_type))

            elif mutation_type in database_constants.OE_mutations:

                output_tuples.append((start, stop, mutation_type))

            elif mutation_type in database_constants.INDEL_mutations:

                # this is skipped because IS insertions are always associated with indels giving the location/size of the
                # mutation.

                if mutation_type == 'is_insertion' and 'indel' not in annotation:
                    output_tuples.append((start, stop, mutation_type))
                else:
                    for (position, size, indel_type) in annotation[mutation_type]:
                        # the locations of some mutations are textually described
                        # skip those
                        if type(position) != int or type(size) != int:
                            continue
                        output_tuples.append((position,
                                              position + size,
                                              mutation_type))

            # aa_snps, nuc_snps, etc
            elif mutation_type in database_constants.RANDOM_mutations:

                if mutation_type == 'nuc_snps':
                    for (location, _, _) in annotation[mutation_type]:
                        if location is None:
                            continue
                        output_tuples.append((location, location + 1, mutation_type))
                elif mutation_type == 'aa_snps':
                    for (aa_location, _, _) in annotation[mutation_type]:
                        if aa_location is None:
                            continue
                        output_tuples.append((start + aa_location * 3, start + aa_location * 3 + 3, mutation_type))

            # large_deletions, large_inversions, large_amplifications
            elif mutation_type in database_constants.REARRANGEMENT_mutations:

                output_tuples.append((start, stop, mutation_type))

    return output_tuples


def get_gene_mutation_tuples(filter_for_mutation_types=None,
                             filter_for_phenotype_class=None):

    """
    
    :param filter_for_mutation_types: 
    :param filter_for_phenotype_class: 
    :return: 
    """

    genotypes = get_mutant_genotypes()

    # denormalize into gene-mutation-phenotype_class

    output_dicts = []

    for genotype in genotypes:

        gene_ids = genotype['gene_ids']
        paired_species = genotype['paired_species_names']
        paired_mg1655 = genotype['paired_mg1655_names']

        mutation_types = genotype['mutations']
        annotations = genotype['annotations']
        pclasses = genotype['pclasses']

        if filter_for_phenotype_class is not None and set(pclasses).isdisjoint(filter_for_phenotype_class):
            continue

        for (gene_id, species_name, mg1655_name, mutation, annotation) in zip(gene_ids, paired_species, paired_mg1655, mutation_types, annotations):

            if filter_for_mutation_types is not None and mutation not in filter_for_mutation_types:
                continue

            output_dicts.append({'species_name': species_name,
                                 'mg1655_name': mg1655_name,
                                 'mutation_type': mutation,
                                 'annotation': annotation,
                                 'gene_id': gene_id,
                                 'strain': genotype['strain']})

    return output_dicts


def get_paper_doe_types():

    cursor.execute('select internal_name as method from resistome.term_explanation where term_type = %s', ('DOE',))

    output_list = []
    for x in cursor:
        output_list.append(x['method'])
    return output_list


def get_mutation_counts_by_doe_type(types_to_include):

    types_to_include = list(types_to_include)

    cursor.execute('select '
                   'count(resistome.annotations.mutation_type) as count '
                   'from resistome.mutations '
                   'inner join resistome.annotations on (resistome.annotations.gene_id = resistome.mutations.gene_id) '
                   'inner join resistome.papers on (resistome.papers.paper_id = resistome.mutations.paper_id) '
                   'where resistome.papers.methods <@ %s '
                   'group by (resistome.mutations.mutant_id)',
                   (types_to_include,))

    mutation_type_counts = cursor.fetchall()

    cursor.execute('select '
                   'count(distinct resistome.mutations.name) as count '
                   'from resistome.mutations '
                   'inner join resistome.papers on (resistome.papers.paper_id = resistome.mutations.paper_id) '
                   'where resistome.papers.methods <@ %s '
                   'group by (resistome.mutations.mutant_id)',
                   (types_to_include,))

    gene_counts = cursor.fetchall()

    return [x['count'] for x in mutation_type_counts], [x['count'] for x in gene_counts]


def mutation_go_tags_by_doe(types_to_include):

    types_to_include = list(types_to_include)

    query = 'select count(distinct <species>.go_terms.go_term) ' \
            'from resistome.mutations ' \
            'inner join <species>.go_terms on (resistome.mutations.name = <species>.go_terms.accession) ' \
            'inner join resistome.papers on (resistome.papers.paper_id = resistome.mutations.paper_id) ' \
            'where resistome.papers.methods <@ %s ' \
            'group by resistome.mutations.mutant_id '

    assembled_query = []
    for s in species:
        assembled_query.append(query.replace('<species>', s))

    assembled_query = ' UNION ALL '.join(assembled_query)
    input_array = []
    for x in species:
        input_array.append(types_to_include)
    cursor.execute(assembled_query, tuple(input_array))

    return [x['count'] for x in cursor]


def gene_essentiality_by_doe(types_to_include):

    types_to_include = list(types_to_include)

    query = 'select SUM(CASE WHEN mg1655.genes.essential = True THEN 1 ELSE 0 END) as recorded_essential ' \
            'from resistome.mutations ' \
            'inner join mg1655.genes on (resistome.mutations.name = mg1655.genes.accession) ' \
            'inner join resistome.papers on (resistome.papers.paper_id = resistome.mutations.paper_id) ' \
            'where resistome.papers.methods <@ %s ' \
            'group by resistome.mutations.mutant_id '

    assembled_query = []
    for s in species:
        assembled_query.append(query.replace('<species>', s))

    assembled_query = ' UNION ALL '.join(assembled_query)
    input_array = []
    for x in species:
        input_array.append(types_to_include)

    cursor.execute(assembled_query, tuple(input_array))

    return [x['recorded_essential'] for x in cursor]


def convert_features_to_genes(features, type_of_feature, query_species='mg1655'):

    if query_species not in species:
        raise AssertionError('Unknown species: %s')

    if type_of_feature == 'go':
        cursor.execute('select accession from ' + query_species + '.go_terms '
                       'where go_term = ANY(%s)', (features,))
    elif type_of_feature == 'metabolite':
        cursor.execute('select accession from ' + query_species + '.metabolomics '
                       'where metabolite = ANY(%s)', (features,))
    elif type_of_feature == 'gene':
        return features
    else:
        raise AssertionError('Unknown feature type: %s' % features)

    return set([x['accession'] for x in cursor])


def convert_genes_to_features(genes, type_of_feature, query_species='mg1655'):

    if query_species not in species:
        raise AssertionError('Unknown species: %s')

    if type_of_feature == 'go':
        cursor.execute('select go_term as feature from ' + query_species + '.go_terms '
                       'where accession = ANY(%s)', (genes,))
    elif type_of_feature == 'metabolite':
        cursor.execute('select metabolite as feature from ' + query_species + '.metabolomics '
                       'where accession = ANY(%s)', (genes,))
    elif type_of_feature == 'gene':
        return genes
    else:
        raise AssertionError('Unknown feature type: %s' % genes)

    return set([x['feature'] for x in cursor])


def convert_phenotype_to_class(phenotype_name):
    """

    Converts a specific phenotype to a general phenotype class (solvents_biofuels, etc).

    :param phenotype_name: 
    :return: 
    """

    cursor.execute('select specific_classes from resistome.phenotype_standardization '
                   'where (resistome.phenotype_standardization.standard_name = %s)',
                   (phenotype_name,))

    return cursor.fetchone()


def get_phenotype_classes():

    """
    
    :return: 
    """

    cursor.execute('select internal_name from resistome.term_explanation '
                   'where upper(term_type) = upper(%s)', ('Tags',))

    categories = set([x['internal_name'] for x in cursor])

    return categories


def convert_full_identifiers_to_abbreviations(abbrev_type, keys):
    """

    Converts a list of keys to standardized representations labeled as abbrev_type.

    Allowed types: 

    categories
    journal
    methods
    mutations
    phenotype

    :param abbrev_type: 
    :param keys: 
    :return: 
    """

    filtered_keys = []
    for o_name in keys:

        cursor.execute(
            'select converted_entry from resistome.abbreviations '
            'where upper(entry_type) = upper(%s) and upper(entry) = UPPER(%s)',
            (abbrev_type, o_name))

        c_name = cursor.fetchone()

        if c_name is None:
            filtered_keys.append(o_name)
        else:
            filtered_keys.append(c_name['converted_entry'])

    return filtered_keys


def distribution_sql(schema, table, field, threshold):

    cursor.execute('select %s from %s.%s' % (field, schema, table))
    count_dict = defaultdict(int)
    value_array = []

    for result in cursor:
        count_dict[result[field].upper()] += 1
        value_array.append(result[field])

    real_journal_dict = defaultdict(int)

    other_value = 0

    for key in count_dict:
        if count_dict[key] <= threshold:
            other_value += count_dict[key]
        else:
            real_journal_dict[key] += count_dict[key]

    key_vals = [(x, y) for x, y in real_journal_dict.iteritems()]
    key_vals = sorted(key_vals, key=lambda x: x[1], reverse=True)
    keys = [x[0] for x in key_vals]
    keys.append('other')
    values = [y[1] for y in key_vals]
    values.append(other_value)

    return keys, values, value_array


def get_table_statistics():

    year_papers = defaultdict(int)
    year_mutation = defaultdict(list)
    year_models = defaultdict(list)

    method_counts = defaultdict(int)
    mutation_counts = defaultdict(int)

    mutant_count_array = []
    mutation_count_array = []
    total_models = []

    gene_usage = defaultdict(int)

    tolerance_phenotypes = defaultdict(int)

    specific_phenotypes = set()

    cursor.execute('select resistome.papers.paper_id, '
                   'array_agg(resistome.paper_tags.tag) as tags,'
                   'resistome.papers.year, '
                   'resistome.papers.designs, '
                   'count(resistome.mutants.mutant_id) as mutant_count,'
                   'array_agg(resistome.mutants.mutant_id) as mutant_ids '
                   'from resistome.papers '
                   'inner join resistome.mutants on (resistome.papers.paper_id = resistome.mutants.paper_id) '
                   'inner join resistome.paper_tags on (resistome.papers.paper_id = resistome.paper_tags.paper_id) '
                   'group by resistome.papers.paper_id')

    for record in cursor.fetchall():

        for tag in record['tags']:
            tolerance_phenotypes[tag] += 1

        year_papers[record['year']] += 1
        total_models.append(record['designs'])
        year_models[record['year']].append(record['mutant_count'])
        mutant_count_array.append(record['mutant_count'])

        cursor.execute('select distinct on (mutant_id, gene_id) '
                       'gene_id, '
                       'name '
                       'from resistome.mutations '
                       'where mutant_id = ANY(%s)', (record['mutant_ids'],))

        gene_results = cursor.fetchall()
        mutation_count_array.append(len(gene_results))
        year_mutation[record['year']].append(len(gene_results))

        for gene in gene_results:
            gene_usage[gene['name']] += 1

    paper_count = sum(year_papers.values())

    cursor.execute('select phenotype from resistome.phenotypes')
    specific_phenotypes.update([x['phenotype'] for x in cursor])

    cursor.execute('select method from resistome.mutant_methods')
    for method in [x['method'] for x in cursor]:
        method_counts[method] += 1

    cursor.execute('select count(*) as model_count from resistome.mutants')

    num_ecoli_designs = cursor.fetchone()['model_count']

    cursor.execute('select distinct on (mutant_id, gene_id) name '
                   'from resistome.mutations '
                   'where upper(species) = upper(%s)',
                   ('Escherichia coli',))
    num_ecoli_genes = len(cursor.fetchall())

    cursor.execute('select mutation_type from resistome.annotations')
    results = cursor.fetchall()
    for info in results:
        mutation_counts[info['mutation_type']] += 1

    cursor.execute('select mutant_id, count(*) from resistome.mutations '
                   'group by mutant_id')
    design_results = cursor.fetchall()

    return year_papers, \
           year_models, \
           year_mutation, \
           paper_count, \
           specific_phenotypes, \
           method_counts, \
           num_ecoli_designs, \
           num_ecoli_genes, \
           design_results, \
           gene_usage, \
           tolerance_phenotypes, \
           mutant_count_array, \
           mutation_count_array, \
           mutation_counts


def get_aggregate_table_data():

    cursor.execute('select '
                   'array_agg(distinct resistome.mutations.name) as species_name, '
                   'array_agg(distinct resistome.gene_standardization.mg1655_accession) as mg1655_name, '
                   'array_agg(distinct resistome.annotations.mutation_type) as mutation_type, '
                   'array_agg(distinct resistome.annotations.annotation) as annotation, '
                   'array_agg(resistome.phenotypes.phenotype) as phenotype, '
                   'array_agg(resistome.phenotypes.phenotype_type) as phenotype_type '
                   'from resistome.mutations '
                   'inner join resistome.annotations on (resistome.mutations.gene_id=resistome.annotations.gene_id) '
                   'inner join resistome.phenotypes on (resistome.mutations.mutant_id=resistome.phenotypes.mutant_id) '
                   'inner join resistome.gene_standardization on (resistome.gene_standardization.species_accession = resistome.mutations.name) '
                   'group by resistome.mutations.gene_id')

    return cursor.fetchall()


def get_tag_statistics():

    category_counter = defaultdict(dict)
    frequency = defaultdict(int)

    cursor.execute('select resistome.papers.year,'
                   'resistome.paper_tags.tag '
                   'from resistome.papers '
                   'inner join resistome.paper_tags on (resistome.paper_tags.paper_id = resistome.papers.paper_id)')

    for result in cursor:

        frequency[result['tag']] += 1
        if result['tag'] in category_counter[result['year']]:
            category_counter[result['year']][result['tag']] += 1
        else:
            category_counter[result['year']][result['tag']] = 1

    return frequency, category_counter


def journal_method_design_distributions(
                                        journal_threshold=5,
                                        method_threshold=10,
                                        mutation_type_threshold=20):
    journal_keys, journal_values, _ = distribution_sql(
                                                       'resistome',
                                                       'papers',
                                                       'journal',
                                                       journal_threshold)

    journal_keys = convert_full_identifiers_to_abbreviations('journal', journal_keys)

    # methods used for mutant design figure
    method_keys, method_values, _ = distribution_sql(
                                                     'resistome',
                                                     'mutant_methods',
                                                     'method',
                                                     method_threshold)
    method_keys = convert_full_identifiers_to_abbreviations('methods', method_keys)

    # mutations made figure
    mutation_keys, mutation_values, _ = distribution_sql(
                                                         'resistome',
                                                         'annotations',
                                                         'mutation_type',
                                                         mutation_type_threshold)

    t_keys = []
    t_values = []

    for key, val in zip(mutation_keys, mutation_values):
        if 'LARGE' not in key:
            t_keys.append(key)
            t_values.append(val)

    keys = t_keys
    mutation_values = t_values

    mutation_keys = convert_full_identifiers_to_abbreviations('mutations', keys)

    return journal_keys, journal_values, method_keys, method_values, mutation_keys, mutation_values

cur_obj = ConnectionManager(username=database_constants.DB_USERNAME,
                            password=database_constants.DB_PASSWORD,
                            db_name=database_constants.DB_NAME)
cursor = cur_obj.cursor