import DatabaseUtils
import os
import psycopg2
import psycopg2.extras
import database_constants

__author__ = 'jdwinkler'

"""

This dict contains the column names for each table in the resistome.resistome postgres schema. It should be kept
consistent with the resistome_sql_schema.sql file in ../inputs . No type annotation is required as inserts are auto-
matically coerced to the type specified in the table definition with the exception of the json dicts used for 
encapsulating annotation data.

"""

column_def = {'papers': ['title', 'doi', 'year', 'research_group', 'journal',
                         'methods', 'score', 'reason', 'genome_reference', 'designs', 'comments'],
              'paper_tags': ['paper_id', 'tag'],
              'mutants': ['paper_id', 'name',
                          'species', 'strain', 'oxygenation',
                          'medium', 'carbon_source', 'media_supplements', 'ph',
                          'vessel_type', 'culture_volume', 'vessel_volume',
                          'temperature', 'rotation', 'fold_improvement',
                          'initial_fitness', 'final_fitness', 'fitness_unit',
                          'comments'],
              'mutant_methods': ['mutant_id', 'method'],
              'mutations': ['paper_id', 'mutant_id', 'name', 'species', 'strain', 'effects', 'original'],
              'expressions': ['paper_id', 'mutant_id', 'study_id', 'name', 'species', 'strain', 'status', 'pvalue',
                              'fold_change'],
              'expression_studies': ['paper_id', 'mutant_id', 'accession', 'measurement_method', 'statistical_method',
                                     'duration', 'growth_phase', 'stressor_level', 'stressor_units'],
              'annotations': ['gene_id', 'mutation_type', 'annotation'],
              'phenotypes': ['mutant_id', 'phenotype', 'phenotype_class', 'phenotype_type', 'ontology_root',
                             'resistance_level', 'resistance_unit'],
              'gene_standardization': ['strain', 'species_accession', 'mg1655_accession'],
            'gene_metadata': ['name', 'species', 'strain', 'species_accession', 'mg1655_accession'],
              'term_explanation': ['term_type', 'internal_name', 'explanation'],
              'phenotype_standardization': ['name', 'standard_name', 'phenotype_type', 'root_class',
                                            'specific_classes'],
              'abbreviations': ['entry', 'entry_type', 'converted_entry'],
              'metabolomics': ['accession', 'metabolite'],
              'gene_ontology': ['accession', 'go_term']}

annotation_format_rules = DatabaseUtils.parse_annotation_format_rules()
subobject_term_rules = DatabaseUtils.parse_input_terms()

species_std = DatabaseUtils.get_species_standard()
unified_std = DatabaseUtils.get_unified_standard()

def insert_sql_get_id(schema, table, columns, id_name):

    """
    
    Appends 'returning ' + the name of the requested column. This is used in sql_generator to provide a way for 
    a method to get the ID associated with an insert for subsequent related inserts.
    
    :param schema: name of the schema (str), usually resistome
    :param table: str, name of the table
    :param columns: iterable of strs, get from column dict
    :param id_name: name of column to return the value of post insert
    :return: formatted SQL query
    """

    sql = prep_sql(schema, table, columns)

    sql += ' returning ' + id_name

    return sql


def prep_sql(schema, table, columns):

    """

    Generates an sql insert query based on the provided inputs.

    :param schema: name of the schema (str), usually resistome
    :param table: str, name of the table
    :param columns: iterable of strs, get from column dict
    :return: formatted SQL query
    """

    return 'insert into ' + schema + '.' + table + ' (' + ', '.join(columns) + ') VALUES ( ' + ','.join(
        ['%s'] * len(columns)) + ')'


def paper_tag_table(cur, paper_id, tags):

    """
    
    Creates the paper tag table (paper id, 'tag' where tag is solvents_biofuels, metal_ions-a category of stress
    phenotype. One ID => many tags. 
    
    :param cur: database cursor to the resistome db
    :param paper_id: unique ID (generated after insert into paper table) for the study
    :param tags: iterable of str with the resistome categorization of the study
    :return: None
    """

    sql = prep_sql('resistome', 'paper_tags', column_def['paper_tags'])

    for tag in tags:
        cur.execute(sql, (paper_id, tag.lower()))


def error_check_annotation(mutation_type, annotation_data):

    """
    
    Checks annotation formats and content for validity.
    
    Ensures bases, residues are valid entries (defined AA/nucleotide bases in database_contents
    Ensures format is consistent for classes of entries
    
    Returns true if a given mutation type, annotation_data pairing are valid, False otherwise.
    
    :param mutation_type: 
    :param annotation_data: 
    :return: 
    
    """

    passes_error_check = True
    value = annotation_data[mutation_type]

    allowed_position_types = {'absolute_inferred', 'absolute', 'gene_unknown'}

    try:
        if mutation_type == 'compartmentalization':
            passes_error_check = True if isinstance(value, str) else False
        elif mutation_type == 'aa_snps':
            for [position, original_aa, mutated_aa] in value:
                if original_aa not in database_constants.AMINO_ACIDS or mutated_aa not in database_constants.AMINO_ACIDS:
                    passes_error_check = False
                if not isinstance(position, int) and position is not None:
                    passes_error_check = False
        elif mutation_type == 'amplified':
            passes_error_check = True if isinstance(value, int) else False
        elif mutation_type == 'antisense':
            passes_error_check = True if isinstance(value, str) or value is None else False
        elif mutation_type == 'scaffold_binder':
            passes_error_check = True if isinstance(value, str) else False
        elif mutation_type == 'truncated':
            passes_error_check = True if isinstance(value, str) or value is None else False
        elif mutation_type == 'codonoptimized':
            passes_error_check = True if isinstance(value, str) or value is None else False
        elif mutation_type == 'con':
            passes_error_check = True if isinstance(value, str) else False
        elif mutation_type == 'large_amplification':
            (first_gene, last_gene, degree_of_amplification) = value
            passes_error_check = True if isinstance(first_gene, str) and \
                                         isinstance(last_gene, str) and \
                                         isinstance(degree_of_amplification, int) else False
        elif mutation_type == 'large_deletion' or mutation_type == 'large_inversion':
            (first_gene, last_gene) = value
            passes_error_check = True if isinstance(first_gene, str) and \
                                         isinstance(last_gene, str)\
                else False
        elif mutation_type == 'del':
            passes_error_check = True if isinstance(value, str) or value is None else False
        elif mutation_type == 'frameshift':
            passes_error_check = True if isinstance(value, str) or value is None else False
        elif mutation_type == 'protein_fusion':
            passes_error_check = True if isinstance(value, str) else False
        elif mutation_type == 'duplication':
            for x in value:
                (start, stop, length, position_type) = (x[0], x[1], x[2], x[3])
                if not isinstance(start, int):
                    passes_error_check = False
                if not isinstance(stop, int):
                    passes_error_check = False
                if not isinstance(length, int):
                    passes_error_check = False
                if position_type not in allowed_position_types:
                    passes_error_check = False
                if isinstance(length, int) and length == 0:
                    passes_error_check = False
        elif mutation_type == 'indel':
            for x in value:
                (position, length, position_type) = (x[0], x[1], x[2])
                length = int(length)
                if not isinstance(position, int) and position is not None:
                    passes_error_check = False
                if not isinstance(length, int):
                    passes_error_check = False
                if position_type not in allowed_position_types:
                    passes_error_check = False
                if isinstance(length, int) and length == 0:
                    passes_error_check = False
        elif mutation_type == 'intergenic':
            first_gene = value[0]
            second_gene = value[1]
            passes_error_check = True if isinstance(first_gene, str) and isinstance(second_gene, str) else False
        elif mutation_type == 'is_insertion':
            (insertion_element, descriptor) = (value[0], value[1])
            if not isinstance(insertion_element, str):
                passes_error_check = False
        elif mutation_type == 'mutated':
            passes_error_check = True if isinstance(value, str) or value is None else False
        elif mutation_type == 'nuc_snps':
            for x in value:
                (position, original_base, mutated_base) = (x[0], x[1], x[2])
                if not isinstance(position, int) and position is not None:
                    passes_error_check = False
                if original_base not in database_constants.NUCLEOTIDE_BASES or mutated_base not in database_constants.NUCLEOTIDE_BASES:
                    passes_error_check = False
        elif mutation_type == 'oe':
            passes_error_check = True if isinstance(value, str) else False
        elif mutation_type == 'plasmid':
            passes_error_check = True if isinstance(value, str) else False
        elif mutation_type == 'promoter_switch':
            passes_error_check = True if isinstance(value, str) else False
        elif mutation_type == 'rbs_tuned':
            passes_error_check = True if isinstance(value, str) else False
        elif mutation_type == 'less_nadh_inhibition':
            passes_error_check = True if isinstance(value, str) else False
        elif mutation_type == 'regulated':
            for x in value:
                passes_error_check = True if isinstance(x, str) else False
                if not passes_error_check:
                    break
        elif mutation_type == 'rep':
            passes_error_check = True if isinstance(value, str) or value is None else False
        elif mutation_type == 'scaffold_bindee':
            passes_error_check = True if isinstance(value, str) else False
        elif mutation_type == 'mrna_secondarystructure':
            passes_error_check = True if isinstance(value, str) or value is None else False
        elif mutation_type == 'sensor':
            passes_error_check = True if isinstance(value, str) else False
        elif mutation_type == 'integrated':
            passes_error_check = True if isinstance(value, str) else False
        elif mutation_type == 'terminated':
            passes_error_check = True if isinstance(value, str) else False
        else:
            raise AssertionError('Unknown mutation type: %s' % mutation_type)

    except:
        passes_error_check = False

    return passes_error_check


def error_check_mutation_entry(entry_data, mutation_types):

    """
    Checks to ensure that all required fields are defined in mutation data, and that they conform to the expected
    format specified in Input Terms_typed.txt.
    
    :param entry_data: tuple of data constructed by insert_mutation_data function
    :param mutation_types: list of mutations encoded in the mutation object
    :return: 
    """

    (paper_id, unique_mutant_id, gene_name, species, converted_strain, effects, original) = entry_data

    passes_error_check = True

    if gene_name is None:
        passes_error_check = False
    if 'Escherichia coli'.upper() not in species.upper():
        passes_error_check = False
    if converted_strain not in database_constants.SPECIES_LIST:
        passes_error_check = False

    for change in mutation_types:
        if change not in annotation_format_rules:
            passes_error_check = False

    return passes_error_check


def error_check_expression_entry(entry_data):

    """

    Checks to ensure that all required fields are defined in expression data, and that they conform to the expected
    format specified in Input Terms_typed.txt.

    :param entry_data: 
    :return: 
    """

    (paper_id, unique_mutant_id, study_id, gene_name, species, converted_strain, ge_change, p_value, fold_change) = entry_data

    passes_error_check = True

    if gene_name is None:
        passes_error_check = False
    if 'Escherichia coli'.upper() not in species.upper():
        passes_error_check = False
    if converted_strain not in database_constants.SPECIES_LIST:
        passes_error_check = False
    if ge_change not in {'+', '-'}:
        passes_error_check = False
    if p_value is not None and not isinstance(p_value, float):
        passes_error_check = False
    if fold_change is not None and not isinstance(fold_change, float):
        passes_error_check = False

    return passes_error_check


def error_check_expression_study(entry_data):

    """

    Checks to ensure that all required fields are defined in expression study data, and that they conform to the expected
    format specified in Input Terms_typed.txt.

    :param entry_data: 
    :return: 
    """

    (paper_id,
     unique_mutant_id,
     accession,
     ge_method,
     stat_test,
     exposure,
     growth_phase,
     stressor,
     units) = entry_data

    passes_error_check = True

    if growth_phase is not None and growth_phase not in database_constants.GROWTH_PHASES:
        passes_error_check = False
    if ge_method is not None and ge_method not in database_constants.EXPRESSION_METHOD:
        passes_error_check = False

    return passes_error_check


def error_check_mutant_entry(entry_data, methods):

    """

    Checks to ensure that all required fields are defined in mutant data, and that they conform to the expected
    format specified in Input Terms_typed.txt.

    :param entry_data: 
    :return: 
    """

    (paper_id,
     mutant_name,
     species,
     converted_strain,
     oxygen,
     medium,
     carbon,
     supplements,
     ph,
     cvessel,
     vvolume,
     cvolume,
     temperature,
     rotation,
     fold_imp,
     init_fit,
     final_fit,
     fit_unit,
     comments) = entry_data

    passes_error_check = True

    if 'Escherichia coli'.upper() not in species.upper():
        passes_error_check = False
    if converted_strain not in database_constants.SPECIES_LIST:
        passes_error_check = False
    if oxygen is not None and oxygen not in database_constants.AIR:
        passes_error_check = False
    if carbon is None:
        passes_error_check = False
    if ph is not None and not isinstance(ph, float):
        passes_error_check = False
    if rotation is not None and not isinstance(rotation, float):
        passes_error_check = False
    if cvolume is not None and not isinstance(cvolume, float):
        passes_error_check = False
    if vvolume is not None and not isinstance(vvolume, float):
        passes_error_check = False
    if temperature is not None and not isinstance(temperature, float):
        passes_error_check = False
    if cvessel is not None and cvessel not in database_constants.CULTURE_VESSELS:
        passes_error_check = False

    for method in methods:
        if method not in database_constants.ENGINEERING_METHODS:
            passes_error_check = False

    return passes_error_check


def error_check_paper_entry(entry_data):

    """

    Checks to ensure that all required fields are defined in paper data, and that they conform to the expected
    format specified in Input Terms_typed.txt.
    
    Note: the duplicate DOI check is located in the ContainerClass method in MetEngDatabase.py.

    :param entry_data: 
    :return: 
    """

    (title, doi, year, group, journal, method, difficulty, reason, reference_genome, total_designs,
     comments) = entry_data

    passes_error_check = True

    if title is None or doi is None:
        passes_error_check = False
    if doi is not None and len(doi) == 0:
        passes_error_check = False
    if journal is None:
        passes_error_check = False
    if group is None:
        passes_error_check = False
    if not isinstance(year, int):
        passes_error_check = False
    if journal is None:
        passes_error_check = False
    if method is None or not set(method).issubset(database_constants.EXPERIMENTAL_DESIGNS):
        passes_error_check = False
    if not isinstance(difficulty, int) or (difficulty < 0 or difficulty > 5):
        passes_error_check = False
    if reason is None or not set(reason).issubset(database_constants.SCORE_REASONS):
        passes_error_check = False

    return passes_error_check


def insert_mutant_phenotype(cur, unique_mutant_id, phenotype, type_of_phenotype, r_level, r_units, tag_dict, root_dict):

    """
    
    Prepares inserts for the phenotype table in the resistome. This table links the mutant entry to data about the 
    phenotype(s) in question, including the resistance level and units.
    
    Tag dict and root dict are defined in the ../inputs/ directory in the Stress Classification.txt and 
    Compound Information.txt. It is assumed that every phenotype/compound encountered in the database is listed
    in both files, otherwise an error is thrown.
    
    :param cur: database cursor for the resistome
    :param unique_mutant_id: unique mutant id after insert into the mutant table
    :param phenotype: str, name of phenotype
    :param type_of_phenotype: str, 'R' for resistant or 'S' for sensitive 
    :param r_level: str, some type of representation about the resistance level (often numeric)
    :param r_units: str, units of r_level
    :param tag_dict: dict (str: str) converts phenotype to its general class
    :param root_dict: dict (str, tuple) describes highest level class of the phenotype
    :return: None
    """

    sql = prep_sql('resistome', 'phenotypes', column_def['phenotypes'])

    error_phenotypes = []

    if phenotype not in tag_dict:
        error_phenotypes.append('Missing phenotype name: %s' % (phenotype,))

    if phenotype is None or len(phenotype) == 0:
        error_phenotypes.append('Missing phenotype?')

    cur.execute(sql, (unique_mutant_id,
                      phenotype.lower(),
                      tag_dict.get(phenotype, phenotype).lower(),
                      type_of_phenotype,
                      root_dict.get(phenotype, 'X')[0],
                      r_level,
                      r_units))

    return error_phenotypes


def insert_annotation(cur, unique_gene_id, annotation, json_dict):

    """
    
    Inserts a json version of the annotations for each mutation into the annotation table. Json is actually a terrible
    type for doing this, but given that annotations take a wide variety of forms and the mutation data have to be 
    handled on a case by case basis anyway, this construction seems to work well enough for now.
    
    Unfortunately the Resistome is gene-centric, so this representation gets a bit awkward in case large non-coding
    regions are mutated.
    
    :param cur: database cursor for the resistome
    :param unique_gene_id: unique id for the associated mutation entry
    :param annotation: str, resistome type of mutation 
    :param json_dict: dict of (str, mutation type: (data)) to contain the annotation
    :return: None
    """

    error_check = error_check_annotation(mutation_type=annotation,
                                         annotation_data=json_dict)

    sql = prep_sql('resistome', 'annotations', column_def['annotations'])
    cur.execute(sql, (unique_gene_id, annotation.lower(), psycopg2.extras.Json(json_dict)))

    return error_check


def insert_mutation_data(cur, year, paper_id, unique_mutant_id, mutation_obj, species, converted_strain):

    """
    
    Inserts mutation data from Resistome records into the mutations table.
    
    :param cur: database cursor for the resistome
    :param paper_id: unique id of the associated study 
    :param unique_mutant_id: unique id of the associated mutant
    :param mutation_obj: a mutation object created by MetEngDatabase when parsing the corresponding .txt file
    :param species: str, species as written in the original record
    :param converted_strain: str, converted species name (REL606, MG1655, W)
    :return: None
    """

    original = mutation_obj.is_original
    effects = mutation_obj.effects
    changes = [x.lower() for x in mutation_obj.changes]

    output_errors = []

    # genes that will be inserted into the table
    # can be quite long for large_(amplification/deletion/inversion) mutations
    genes_to_add = list(mutation_obj.modified_genes)

    if len(changes) == 0:
        output_errors.append('Missing mutation types (GeneMutation field)?')

    if 'large_amplification' in changes:
        changes = ['large_amplification']
        try:
            # converts the mutation into gene1, geneN, amount of amplification
            mutation_obj.annotation_backing['large_amplification'] = (mutation_obj.annotation('large_amplification')[0],
                                                                      mutation_obj.annotation('large_amplification')[
                                                                          -2],
                                                                      int(mutation_obj.annotation(
                                                                          'large_amplification')[-1]))
        except (ValueError, TypeError):
            # this sometimes occurs if the value in the file is actually a float
            # though a sane language would probably just truncate the .0...
            mutation_obj.annotation_backing['large_amplification'] = (mutation_obj.annotation('large_amplification')[0],
                                                                      mutation_obj.annotation('large_amplification')[-2],
                                                                      int(float(mutation_obj.annotation(
                                                                          'large_amplification')[-1])))

    elif 'large_deletion' in changes:

        changes = ['large_deletion']
        # converts the mutation into gene1, geneN
        mutation_obj.annotation_backing['large_deletion'] = (mutation_obj.annotation_backing['large_deletion'][0],
                                                             mutation_obj.annotation_backing['large_deletion'][-1])

    elif 'large_inversion' in changes:
        # converts the mutation into gene1, geneN
        mutation_obj.annotation_backing['large_inversion'] = (mutation_obj.annotation_backing['large_inversion'][0],
                                                              mutation_obj.annotation_backing['large_inversion'][-1])

    for gene_name in genes_to_add:

        data_tuple = (paper_id,
                      unique_mutant_id,
                      gene_name.upper().replace('\'', ''),
                      species,
                      converted_strain.lower(),
                      effects,
                      original)

        valid_mutation_object = error_check_mutation_entry(data_tuple, changes)

        if not valid_mutation_object:
            output_errors.append('Error in mutation object: %s, %s' % (str(data_tuple), str(changes)))

        # get unique id to associate the annotation with the proper mutation
        sql = insert_sql_get_id('resistome', 'mutations', column_def['mutations'], 'gene_id')
        cur.execute(sql, data_tuple)
        gene_id = cur.fetchone()[0]

        # get gene start from biocyc (#converted_strain#.genes) where converted_strain is
        # also the name of a biocyc table.
        cur.execute('select * from ' + converted_strain + '.genes where upper(accession) = %s',
                    (gene_name,))
        gene = cur.fetchone()
        if gene is None:
            gene_start = 0
            gene_end = 0
        else:
            gene_start = int(gene[6])
            gene_end = int(gene[7])

        for change in changes:

            annotation = mutation_obj.annotation(change)

            if change == 'nuc_snps':
                # all locations are absolute by default
                # todo: add in genome reference correction based on year (2013)
                annotation = [x.replace('|absolute', '') for x in annotation]

                annotation_tuples = []

                proper_location_tuple = database_constants.get_proper_start_stop(gene_name.upper(),
                                                                                 year,
                                                                                 query_species=converted_strain.lower())

                if proper_location_tuple is not None:
                    (ref_gene_start, ref_gene_end) = proper_location_tuple
                else:
                    ref_gene_start = 0
                    ref_gene_end = 0

                for entry in annotation:

                    original_value = entry[0]
                    altered_value = entry[-1]
                    location = entry[1:-1]

                    if ref_gene_end != 0 and ref_gene_start != 0:
                        relative_location = database_constants.convert_to_relative_position(ref_gene_start,
                                                                                            ref_gene_end,
                                                                                            location)
                        location = database_constants.convert_to_absolute_position(gene_start,
                                                                                   gene_end,
                                                                                   relative_location)
                    else:
                        location = database_constants.convert_to_absolute_position(gene_start, gene_end, location)

                    annotation_tuples.append((location, original_value, altered_value))

                annotation = annotation_tuples

            if change == 'aa_snps':

                annotation_tuples = []

                for entry in annotation:

                    original_value = entry[0].upper()
                    altered_value = entry[-1].upper()
                    location = entry[1:-1]

                    try:
                        # fix zero indexing
                        location = int(location) - 1
                    except (ValueError, TypeError):
                        # if location is not actually given (very rare)
                        location = None
                    annotation_tuples.append((location, original_value, altered_value))
                annotation = annotation_tuples

            if change == 'indel':
                output_tuples = []
                for (location, size, type_of_reference) in annotation:
                    if type_of_reference == 'relative' or type_of_reference is None:
                        if gene is None:
                            if location == 'coding':
                                location = None
                            output_tuples.append((location, size, 'gene_unknown'))
                            continue
                        if gene_start != 0 and gene_end != 0:
                            actual_location = database_constants.convert_to_absolute_position(gene_start, gene_end, location)
                            location_type = 'absolute'
                        else:
                            actual_location = int((float(gene_start) + float(gene_end)) / 2.0)
                            location_type = 'absolute_inferred'
                        output_tuples.append((actual_location, size, location_type))

                    else:
                        if location == 'coding':
                            location = None
                        output_tuples.append((location, size, type_of_reference))

                annotation = output_tuples

            if change == 'duplication':

                output_tuples = []

                (location, size, type_of_reference) = annotation
                location_tokens = location.split('-')
                start = int(location_tokens[0])
                stop = int(location_tokens[1])
                size = int(size)

                if type_of_reference == 'relative' or type_of_reference is None:

                    if gene is None:
                        output_tuples.append((location, size, 'gene_unknown'))
                        continue

                    actual_start_location = database_constants.convert_to_absolute_position(gene_start, gene_end, start)
                    actual_stop_location = database_constants.convert_to_absolute_position(gene_start, gene_end, stop)
                    location_type = 'absolute'
                    output_tuples.append((actual_start_location, actual_stop_location, size, location_type))
                else:
                    output_tuples.append((start, stop, size, type_of_reference))

                annotation = output_tuples

            if change == 'oe':
                annotation = annotation[0]

            if change == 'plasmid':
                annotation = annotation[0]

            wrapper_dict = {change: annotation}
            valid = insert_annotation(cur, gene_id, change, wrapper_dict)

            if not valid:
                output_errors.append(('Error detected in %s: %s (%s)' % (change, str(wrapper_dict[change]),
                                                                         gene_name)))

    return output_errors


def insert_expression_data(cur, paper_id, unique_mutant_id, study_id, expression_obj, species, converted_strain):

    """
    
    Inserts expression data into the expressions table. Conceptually very similar to the mutations table,
    but without a companion 'annotations' table.
    
    :param cur: database cursor for the resistome
    :param paper_id: unique id for the study
    :param unique_mutant_id: unique id for the mutant
    :param study_id: unique id for the expression study 
    :param expression_obj: an object describing the fields associated with annotated resistome expression data
    :param species: original species name specified in Resistome record
    :param converted_strain: converted strain name (mg1655, rel606, w)
    :return: 
    """

    errors = []

    gene_name = expression_obj.name
    try:
        ge_change = '+' if expression_obj.gechange.lower() == 'overexpressed' else '-'
    except:
        # this occurs if gechange is missing
        if expression_obj.foldchange > 1.0:
            ge_change = '+'
        else:
            ge_change = '-'
    p_value = expression_obj.pvalue
    fold_change = expression_obj.foldchange

    data_tuple = (paper_id,
                  unique_mutant_id,
                  study_id,
                  gene_name,
                  species,
                  converted_strain,
                  ge_change,
                  p_value,
                  fold_change)

    valid_expression_object = error_check_expression_entry(data_tuple)

    if not valid_expression_object:
        errors.append('Error in expression object data: %s' % str(data_tuple))

    sql = prep_sql('resistome', 'expressions', column_def['expressions'])
    cur.execute(sql, data_tuple)

    return errors


def insert_mutant(cur, year, paper_id, mutant_obj, tag_info, root_classes):

    """
    
    Inserts a mutant record into the mutants table in the resistome. This method does the lion's share of parsing,
    as it drives inserts into the phenotype, methods, mutations, and expression_studies/expression tables
    since they are all based on connections to the unique mutant genotype being inserted here.
    
    :param cur: database cursor for the resistome
    :param paper_id: unique id associated with the study
    :param mutant_obj: object created to represent a given mutant entry by MetEngDatabase
    :param tag_info: dict (str : str) to annotate specific phenotypes with their general class
    :param root_classes: dict (str : tuple) higher level hierachy to describe phenotypes
    :return: None
    """

    errors = []

    mutant_name = mutant_obj.name
    species = mutant_obj.species
    strain = mutant_obj.strain

    # culture vessel, options listed in Term Usage.txt under ../inputs/
    cvessel = mutant_obj.culture_vessel

    # oxygenation
    oxygen = mutant_obj.oxygenation
    (medium, supplements) = mutant_obj.medium
    carbon = mutant_obj.substrates
    ph = mutant_obj.pH
    cvolume = mutant_obj.culture_volume
    vvolume = mutant_obj.liquid_volume
    temperature = mutant_obj.temperature
    rotation = mutant_obj.rpm
    fold_imp = mutant_obj.fold_improvement
    init_fit = mutant_obj.initial_fitness
    final_fit = mutant_obj.final_fitness
    fit_unit = mutant_obj.fitness_unit

    # comments is a free-form text field not used for anything currently
    comments = mutant_obj.comments

    methods = mutant_obj.methods

    resist_level = mutant_obj.resistance_level
    resist_units = mutant_obj.resistance_units

    sensitive_phenotypes = mutant_obj.sensitive_phenotypes
    resistant_phenotypes = mutant_obj.resistant_phenotypes

    converted_strain = database_constants.get_strain_converter(strain)

    mutant_tuple = (paper_id,
                    mutant_name,
                    species,
                    converted_strain,
                    oxygen,
                    medium,
                    carbon,
                    supplements,
                    ph,
                    cvessel,
                    vvolume,
                    cvolume,
                    temperature,
                    rotation,
                    fold_imp,
                    init_fit,
                    final_fit,
                    fit_unit,
                    comments)

    valid_mutant_object = error_check_mutant_entry(mutant_tuple, methods)

    if not valid_mutant_object:
        errors.append('Error detected in high-level mutant object: %s, %s' % (str(mutant_tuple), str(methods)))

    sql = insert_sql_get_id('resistome', 'mutants', column_def['mutants'], 'mutant_id')
    cur.execute(sql, mutant_tuple)
    unique_mutant_id = cur.fetchone()[0]

    study_id = None

    if len(mutant_obj.expression) > 0:
        expression_obj = mutant_obj.expression[0]

        stat_test = expression_obj.stattest
        exposure = expression_obj.exposure
        growth_phase = expression_obj.growthphase
        ge_method = expression_obj.gemethod
        stressor = expression_obj.stress_amount
        units = expression_obj.stress_units
        accession = expression_obj.accession

        if growth_phase is not None:
            growth_phase = growth_phase.lower()

        if ge_method is not None:
            ge_method = ge_method.lower()

        high_level_descriptor = (paper_id,
                                 unique_mutant_id,
                                 accession,
                                 ge_method,
                                 stat_test,
                                 exposure,
                                 growth_phase,
                                 stressor,
                                 units)

        valid_expression_study = error_check_expression_study(high_level_descriptor)

        if not valid_expression_study:
            errors.append('Error in expression study: %s' % str(high_level_descriptor))

        sql = insert_sql_get_id('resistome', 'expression_studies', column_def['expression_studies'], 'study_id')
        cur.execute(sql, high_level_descriptor)
        study_id = cur.fetchone()[0]

    for method in methods:
        sql = prep_sql('resistome', 'mutant_methods', column_def['mutant_methods'])
        cur.execute(sql, (unique_mutant_id, method))

    for phenotype in sensitive_phenotypes:
        errors.extend(insert_mutant_phenotype(cur, unique_mutant_id, phenotype, 'S',
                                resist_level, resist_units, tag_info, root_classes))

    for phenotype in resistant_phenotypes:
        errors.extend(insert_mutant_phenotype(cur, unique_mutant_id, phenotype, 'R',
                                resist_level, resist_units, tag_info, root_classes))

    affected_genes = []

    for mutation in mutant_obj.mutations:
        affected_genes.extend(mutation.modified_genes)
        errors.extend(insert_mutation_data(cur, year, paper_id, unique_mutant_id, mutation, species, converted_strain))

    for expression in mutant_obj.expression:
        errors.extend(insert_expression_data(cur, paper_id, unique_mutant_id, study_id, expression, species,
                                             converted_strain))

    return errors


def build_gene_standardization_table(cur, papers):

    """
    
    Builds the gene standardization table (essentially tuples of E. coli species, accession, and the mg1655 equivalent
    if there is one).
    
    :param cur: database cursor for the resistome
    :param papers: collection of paper objects (paper with [mutant : [mutations]]
    :return: None
    """

    conversion_table_sql = prep_sql('resistome', 'gene_standardization', column_def['gene_standardization'])
    metadata_table_sql = prep_sql('resistome', 'gene_metadata', column_def['gene_metadata'])

    added_already = set()

    in_standard = set(species_std.standard.values())

    from collections import defaultdict
    multiple_conversion = defaultdict(list)

    # extract standardization data from 'Species Specific Gene Names.txt' in ../inputs/
    for (gene, species, strain) in species_std.standard:
        standardized_name = species_std.convert(gene, species, strain=strain)
        mg1655_name = unified_std.convert(gene, species, strain=strain)

        multiple_conversion[(gene, strain)].append((standardized_name, mg1655_name))

        if (strain, standardized_name) not in added_already:
            cur.execute(conversion_table_sql, (strain, standardized_name, mg1655_name))

        cur.execute(metadata_table_sql, (gene, species, strain, standardized_name, mg1655_name))
        added_already.add((strain, standardized_name))

    for (gene, strain) in multiple_conversion:

        if len(multiple_conversion[(gene, strain)]) > 1:
            print gene, strain, multiple_conversion[(gene, strain)]

    for paper in papers:
        for mutant in paper.mutants:
            strain = database_constants.get_strain_converter(mutant.strain)
            for mutation in mutant.mutations:
                for gene in mutation.modified_genes:
                    standardized_name = species_std.convert(gene, mutant.species, strain=strain)
                    # adds anything that isn't covered by the standardization file (assumes it is a weirdly named
                    # gene-not very common, but often enough to handle
                    if gene.upper() == standardized_name.upper() and gene.upper() not in in_standard and (strain.upper(), gene) not in added_already:
                        cur.execute(metadata_table_sql, (gene, mutant.species, strain, gene, gene))
                        cur.execute(conversion_table_sql, (strain.upper(), gene, gene))
                        added_already.add((strain.upper(), gene))


def build_phenotype_standardization_table(cur):

    """
    
    Builds the phenotype standardization table compiling all phenotypes in the resistome and their associated
    categories/classifications.
    
    :param cur: database cursor for the resistome
    :return: None
    """

    phenotype_std = DatabaseUtils.get_phenotype_standard()

    tag_dict, ontology = DatabaseUtils.load_tolerance_ontology()

    for (tag, _, _) in phenotype_std.standard:

        standardized_tag = ''

        try:
            # try to convert the phenotype to something more standardized
            standardized_tag = phenotype_std.convert(tag, 'None')

            # if the above fails, tries the original tag (might already by the standard form)
            tag_category = tag_dict.get(standardized_tag, tag_dict.get(tag, None))

            # root_class is something like chemical or environmental stress, specific classes as you'd expect
            (root_class, specific_classes) = ontology.get(standardized_tag, ontology.get(tag, None))
        except:
            raise ValueError('Missing stress ontology tag: %s, %s' % (standardized_tag, tag))

        sql = prep_sql('resistome', 'phenotype_standardization', column_def['phenotype_standardization'])
        cur.execute(sql, (tag.lower(),
                          standardized_tag.lower(),
                          tag_category.lower(),
                          root_class.lower(),
                          list(specific_classes)))


def build_term_explanation_table(cur):

    """
    
    Loads the term data originally used for the resistome local data entry tool into a table. This is mainly for
    user reference and is not used anywhere.
    
    :param cur: database cursor for the resistome
    :return: None
    """

    with open(os.path.join(database_constants.INPUT_DIR, 'settings', 'Term Usage.txt'), 'rU') as fhandle:
        for line in fhandle:
            tokens = line.strip().split('\t')
            position = tokens[0]
            internal_name = tokens[1]

            # public explanation is the converted text display to the user, so often it is less of an explanation
            # and more of an interrogatory statement
            public_explanation = tokens[2]

            sql = prep_sql('resistome', 'term_explanation', column_def['term_explanation'])
            cur.execute(sql, (position, internal_name, public_explanation))


def build_abbreviation_tables(cur):

    """
    
    Centralizes the documents describing various forms of abbreviations for phenotypes, experimental methods,
    journals, mutations, and general phenotype categories.
    
    :param cur: database cursor for the resistome
    :return: None
    """

    files = [('category_abbrevs.txt', 'categories'),
             ('journal_abbrevs.txt', 'journal'),
             ('method_abbrevs.txt', 'methods'),
             ('mutation_abbrevs.txt', 'mutations'),
             ('tolerance_abbrevs.txt', 'phenotype')]

    for (file_x, table_type) in files:

        with open(os.path.join(database_constants.INPUT_DIR, 'settings', file_x)) as f:

            f.readline()
            for line in f:
                tokens = line.strip().split('\t')
                entry = tokens[0].upper()
                conversion = tokens[1].upper()

                sql = prep_sql('resistome', 'abbreviations', column_def['abbreviations'])
                cur.execute(sql, (entry.upper(), table_type, conversion.upper()))


def build_go_metabolite_tables(cur):
    """

    Creates two tables containing gene accession to either metabolite (based on Zampieri et al 2017, MSB, Metabolic
    Constraints on Antibiotic Resistance) or gene ontology annotations per gene from Biocyc. 

    :param cur: database cursor for the resistome
    :return: None
    """

    with open(os.path.join(database_constants.INPUT_DIR, 'metabolomics', 'mg1655_accession_to_metabolite.txt'), 'rU') as f:

        for line in f:
            tokens = line.strip().split('\t')
            accession = tokens[0]
            metabolite = tokens[1]

            # note that the Zampieri study only has MG1655 data, and it is probably a bad idea to extrapolate
            # to other subspecies without explicit user acknowledgement of that fact
            sql = prep_sql('resistome', 'metabolomics', column_def['metabolomics'])
            cur.execute(sql, (accession, metabolite))

    with open(os.path.join(database_constants.INPUT_DIR, 'ontologies', 'accession_to_go_terms.txt'), 'rU') as f:

        for line in f:
            tokens = line.strip().split('\t')
            accession = tokens[0]
            metabolite = tokens[1]
            sql = prep_sql('resistome', 'gene_ontology', column_def['gene_ontology'])
            cur.execute(sql, (accession, metabolite))


def main(add_resistome_go_metabolite_tables=False):

    from collections import defaultdict

    tags_information, ontology = DatabaseUtils.load_tolerance_ontology()

    # manually get cursor to avoid depending on Database utils for this
    # also, you need to run the biocyc_data_parser to build the biocyc tables first

    try:
        connect = psycopg2.connect("dbname='%s' user='%s' host='localhost' password='%s'" % (database_constants.DB_NAME,
                                                                                             database_constants.DB_USERNAME,
                                                                                             database_constants.DB_PASSWORD))
    except:
        raise

    cur = connect.cursor()

    cur.execute('drop schema if exists resistome cascade')

    with open(os.path.join(database_constants.INPUT_DIR, 'sql', 'resistome_sql_schema.sql'), 'rU') as f:
        sql_schema = ''.join(f.readlines())
        cur.execute(sql_schema)

    # other databases currently only includes Carol Gross' large phenotyping effort for the Keio collection
    standardized_papers = DatabaseUtils.get_database(folders=('2018', '2017', '2016', '2015', 'other_databases'))
    # standardized_papers = DatabaseUtils.get_database(folders=('test',))

    print 'Inserting Resistome Data into SQL database'

    error_dict = defaultdict(set)

    doi_duplicate_tracker = set()

    for paper in standardized_papers:

        title = paper.title
        doi = paper.doi
        year = paper.year
        group = paper.rgroup
        journal = paper.journal
        difficulty = paper.difficulty
        reason = paper.difficulty_reason
        method = paper.design_method
        tags = paper.categories
        total_designs = paper.total_designs
        comments = paper.comments

        # this check is in MetEngDatabase as well
        if doi in doi_duplicate_tracker:
            error_dict[title].add('Duplicate DOI detected: %s' % doi)

        if difficulty is None:
            difficulty = 1

        # human = none based on Terms Usage.txt
        if 'human' in method:
            method = set(method)
            method.remove('human')
            method.add('none')
            method = list(method)

        reference_genome = paper.reference_genome()

        if total_designs is None:
            total_designs = len(paper.mutants)

        paper_tuple = (title, doi, year, group, journal, method, difficulty, reason, reference_genome, total_designs,
                       comments)

        valid_paper_object = error_check_paper_entry(paper_tuple)

        if not valid_paper_object:
            error_dict[title].add('Error in paper level object: %s' % str(paper_tuple))

        sql = insert_sql_get_id('resistome',
                                'papers',
                                column_def['papers'],
                                'paper_id')

        cur.execute(sql, paper_tuple)
        inserted_id = cur.fetchone()[0]

        paper_tag_table(cur, inserted_id, tags)

        for mutant in paper.mutants:

            paper_errors = insert_mutant(cur, year, inserted_id, mutant, tags_information, ontology)
            if len(paper_errors) > 0:
                error_dict[title].update(paper_errors)

    if len(error_dict.values()) > 0:

        for title in error_dict:
            if len(error_dict[title]) > 0:
                print 'Paper title: %s' % title
                for e in error_dict[title]:
                    print e

        raise AssertionError('Errors detected in data corpus; aborting insert transaction.')

    build_gene_standardization_table(cur, standardized_papers)

    build_phenotype_standardization_table(cur)

    build_term_explanation_table(cur)

    build_abbreviation_tables(cur)

    if add_resistome_go_metabolite_tables:
        build_go_metabolite_tables(cur)

    print 'Finished loading data into resistome SQL schema.'

    connect.commit()
    connect.close()


if __name__ == '__main__':
    main(add_resistome_go_metabolite_tables=True)
