create schema public

create table go_table (

	go_term	text unique not null,
	ancestors	text[] not null,
	name	text not null

);

create index go_tag_idx on go_table (go_term);

create table go_adj_list (

	parent	text not null,
	child	text not null

);

create index go_adj_source_idx on go_adj_list ( parent );
create index go_adj_sink_idx on go_adj_list ( child );

create table ms_ions (

	ion_id	text unique not null,
	kegg_id	text,
	name	text
);

create index ms_idx on ms_ions (ion_id);

create table genetic_code (

	codon	varchar(3) not null,
	aa		varchar(1) not null,
	frequency	real not null

);