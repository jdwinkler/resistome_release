create schema <species>

create table <species>.genes (

	gene_id	serial primary key,
	biocyc_id	text unique not null,
	name	text not null,
	synonyms	text[] default array[]::text[],
	products	text[] default array[]::text[],
	accession	text unique not null,
	start	integer not null,
	stop	integer not null,
	direction	varchar(1) not null,
	essential	boolean not null
	
	constraint transdir_chk check (direction = '-' or direction = '+')
	
);

create index name_idx on <species>.genes (name);
create index accession_idx on <species>.genes (accession);

create table <species>.genome (

	chromosome	text primary key,
	sequence	text not null

);

create table <species>.metabolomics (

	accession	text not null,
	metabolite	text not null
	
);

create index met_acc_idx on <species>.metabolomics(accession);

create table <species>.protein_stability (

	stable_id	serial primary key,
	accession	text not null,
	position	smallint	not null,
	wt_aa	varchar(1) not null,
	mutant_aa	varchar(1) not null,
	score	float not null,
	method	text not null

);

create table <species>.go_terms (

	accession	text not null,
	go_term	text not null,
	
	constraint accession_fk foreign key (accession) references <species>.genes (accession),
	constraint go_term_fk foreign key (go_term) references public.go_table (go_term)
);

create index go_acc_idx on <species>.go_terms(accession);

create table <species>.uniprot (

	accession	text not null,
	region	text not null,
	start	integer not null,
	stop	integer not null,
	note	text,

	constraint accession_fk foreign key (accession) references <species>.genes (accession)

);

create table <species>.interactions (

	interaction_type	text not null,
	regulator	text not null,
	target	text not null,
	direction varchar(2) not null,
	db_source	text not null
	
	constraint transdir_chk check (direction = '-' or direction = '+' or direction = '?' or direction = '+-')

);

create index regulator_idx on <species>.interactions (regulator);
create index target_idx on <species>.interactions (target);

create index uniprot_acc_idx on <species>.uniprot(accession);

create table <species>.aa_sequences (

	accession	text not null,
	aa_seq	text not null,
	
	constraint accession_fk foreign key (accession) references <species>.genes (accession)

);

create index aa_acc_idx on <species>.aa_sequences(accession);


create table <species>.nt_sequences (

	accession	text not null,
	nt_seq	text not null,
	
	constraint accession_fk foreign key (accession) references <species>.genes (accession)
);

create index nt_acc_idx on <species>.nt_sequences(accession);

create table <species>.genomic_features (

	feature_id serial primary key,
	biocyc_id	text unique not null,
    feature_type	text not null,
    start	integer not null,
    stop	integer not null,
	
	constraint feature_check check (feature_type = 'terminator' or feature_type = 'promoter' or feature_type = 'dbs')
    
);

create table <species>.feature_regulators (

	interaction_id serial primary key,
	feature_id integer not null,
	name	text not null,
	
	constraint feature_id_fk foreign key (feature_id) references <species>.genomic_features(feature_id),
	constraint regulator_fk foreign key (name) references <species>.genes (accession)
   
);

create table <species>.feature_regulated_genes (

	interaction_id serial primary key,
	feature_id integer not null,
	name	text not null,
	
	constraint feature_id_fk foreign key (feature_id) references <species>.genomic_features(feature_id),
	constraint regulatee_fk foreign key (name) references <species>.genes (accession)
   
);




