create schema resistome

create table resistome.papers (
	
	paper_id	serial not null,
	title	text not null,
	doi     text UNIQUE not null,
	year	smallint not null,
	research_group	text not null,
	journal	text not null,
	methods text[] not null,
	score	smallint not null,
	reason	text[] not null,
	genome_reference	text,
	designs	bigint not null,
	comments	text,
	
	constraint paper_pkey primary key (paper_id)
);

create index papers_idx on resistome.papers (paper_id);

create table resistome.paper_tags (

	paper_id integer not null,
	tag	text not null,

	CONSTRAINT fk_paper_id FOREIGN KEY ( paper_id ) REFERENCES resistome.papers ( paper_id )

);

create index tag_index on resistome.paper_tags (tag);
create index pid_index on resistome.paper_tags (paper_id);

create table resistome.mutants (

	mutant_id	serial primary key,
	paper_id	integer not null,
	name	text not null,
	species	text not null,
	strain	text not null,
	oxygenation	text,
	medium	text,
	carbon_source	text[],
	media_supplements	text[],
	ph	real,
	vessel_type	text,
	culture_volume	real,
	vessel_volume	real,
	temperature	real,
	rotation	real,
	fold_improvement	real,
	initial_fitness	real,
	final_fitness	real,
	fitness_unit	text,
	comments	text,
	
	CONSTRAINT fk_paper_id FOREIGN KEY ( paper_id ) REFERENCES resistome.papers ( paper_id )
	
);

create index mutant_id_idx on resistome.mutants (mutant_id);

create table resistome.mutant_methods (

	mutant_id	integer not null,
	method text not null,
	
	CONSTRAINT fk_source_id FOREIGN KEY ( mutant_id ) REFERENCES resistome.mutants(mutant_id)

);

create table resistome.phenotypes (

	mutant_id	integer not null,
	phenotype	text not null,
	phenotype_class	text not null,
	phenotype_type	varchar(1) not null,
	ontology_root	text not null,
	resistance_level text,
	resistance_unit text,
	
	CONSTRAINT fk_source_id FOREIGN KEY ( mutant_id ) REFERENCES resistome.mutants(mutant_id),
	
	constraint phenotype_type_chk check (phenotype_type = 'R' or phenotype_type = 'S')
	
);

create index combined_phenotypes_idx on resistome.phenotypes (mutant_id);


create table resistome.mutations (

	paper_id	integer not null,
	mutant_id	integer not null,
	gene_id	serial primary key,
	name	text not null,
	species	text not null,
	strain	text not null,
	effects text[] not null,
	original	boolean not null,
	
	CONSTRAINT fk_source_id FOREIGN KEY ( mutant_id ) REFERENCES resistome.mutants(mutant_id),
	CONSTRAINT fk_source2_id FOREIGN KEY ( paper_id ) REFERENCES resistome.papers(paper_id),
	constraint unique_gene_name_pairing unique (gene_id, name)

);

create index combined_mutation_idx on resistome.mutations (mutant_id);


create table resistome.expression_studies (

	paper_id	integer not null,
	mutant_id	integer not null,
	study_id	serial primary key,
	accession	text,
	measurement_method	text,
	statistical_method	text,
	duration	text,
	growth_phase	text,
	stressor_level	text,
	stressor_units	text,
	
	CONSTRAINT fk_source_id FOREIGN KEY ( mutant_id ) REFERENCES resistome.mutants(mutant_id),
	CONSTRAINT fk_source2_id FOREIGN KEY ( paper_id ) REFERENCES resistome.papers(paper_id)
	
);

create table resistome.expressions (

	paper_id	integer not null,
	mutant_id	integer not null,
	study_id	integer not null,
	gene_id	serial primary key,
	name	text not null,
	species	text not null,
	strain	text not null,
	status varchar(1) not null,
	pvalue	double precision,
	fold_change	double precision,
	
	CONSTRAINT fk_source_id FOREIGN KEY ( mutant_id ) REFERENCES resistome.mutants(mutant_id),
	CONSTRAINT fk_source2_id FOREIGN KEY ( paper_id ) REFERENCES resistome.papers(paper_id),
	CONSTRAINT fk_source3_id FOREIGN KEY ( study_id ) REFERENCES resistome.expression_studies(study_id),
	constraint transdir_chk check (status = '-' or status = '+')
	
);

create index expression_idx on resistome.expressions (mutant_id);


create table resistome.annotations (

	annotation_id	serial primary key,
	gene_id	integer not null,
	mutation_type text not null,
	annotation	jsonb not null,
	
	constraint fk_gene_id foreign key (gene_id) references resistome.mutations (gene_id)

);

create index annotations_idx on resistome.annotations (gene_id);

create table resistome.gene_standardization (

	strain	text	not null,
	species_accession	text not null,
	mg1655_accession	text not null,
	
	constraint unique_tuple_gs unique (strain, species_accession)

);

CREATE INDEX gs_name_idx ON resistome.gene_standardization (species_accession);

create table resistome.gene_metadata (

	name	text	not null,
	species	text	not null,
	strain	text	not null,
	species_accession	text not null,
	mg1655_accession	text not null,
	
	constraint unique_tuple_gmd unique (name, species, strain)

);

CREATE INDEX gmd_name_idx ON resistome.gene_metadata (name);

create table resistome.phenotype_standardization (

	name	text not null,
	standard_name	text not null,
	phenotype_type	text not null,
	root_class	text not null,
	specific_classes	text[] not null

);

CREATE INDEX ps_name_idx on resistome.phenotype_standardization (name);
CREATE INDEX ps_std_idx on resistome.phenotype_standardization (standard_name);

create table resistome.term_explanation (

	term_type	text not null,
	internal_name	text not null,
	explanation	text not null

);

create table resistome.abbreviations (

	entry	text not null,
	entry_type text not null,
	converted_entry	text not null,
	
	constraint entry_type_unique unique (entry, entry_type)

);

create table resistome.metabolomics (

	accession	text not null,
	metabolite	text not null
	
);

create index metab_idx on resistome.metabolomics (accession);

create table resistome.gene_ontology (

	accession	text not null,
	go_term	text not null

);

create index go_idx on resistome.gene_ontology (accession);