# enzymes.col: describes enzyme:reaction relationships
# reactions.dat can also be used in a similar way (probably better to use)
# genes dat: unique IDs for gene products + their synonyms
# proteins dat: unique ids for reactions from gene products
#
# parsing pattern:
#
# Key _ Value (note that value contains the delimiter '_' on many occasions)
#
# parsing rule:
#
# str.split("_",1), where 1 indicates at most 1 split at the first _ occurrence

# record separator: //

from collections import defaultdict
import os.path
import re
import database_constants
import DatabaseUtils
import math
import psycopg2
import psycopg2.extras

import logging
log = logging.getLogger(__name__)

entries = dict()
entries['genes'] = ['biocyc_id',
                    'name',
                    'synonyms',
                    'products',
                    'accession',
                    'start',
                    'stop',
                    'direction',
                    'essential']
entries['dna'] = ['accession', 'nt_seq']
entries['aa'] = ['accession', 'aa_seq']
entries['genome'] = ['chromosome', 'sequence']
entries['uniprot'] = ['accession', 'region', 'start', 'stop', 'note']
entries['go_terms'] = ['accession', 'go_term']
entries['interactions'] = ['interaction_type',
                           'regulator',
                           'target',
                           'direction',
                           'db_source']
entries['public_go'] = ['go_term',
                        'ancestors',
                        'name']
entries['go_adj_list'] = ['parent', 'child']
entries['genetic_code'] = ['codon', 'aa', 'frequency']
entries['genomic_features'] = ['biocyc_id', 'feature_type', 'start', 'stop']
entries['feature_regulators'] = ['feature_id', 'name']
entries['feature_regulated_genes'] = ['feature_id', 'name']

gene_data = 'genes.dat'
protein_data = 'proteins.dat'
rna_data = 'rnas.dat'
classes = 'classes.dat'
protein_sequence = 'protseq.fsa'
dna_sequence = 'dnaseq.fsa'
genome = 'genome.nt'
uniprot = 'uniprot.gff'
regulon_db = 'regulon_db_standardized.txt'
ecolinet = 'EcoliNet.v1.txt'
dna_binding_sites = 'dnabindsites.dat'
promoters = 'promoters.dat'
terminators = 'terminators.dat'
transcriptional_units = 'transunits.dat'
regulation = 'regulation.dat'


def prepare_sql_query(table, schema, columns, arguments=None):

    """
    
    Prepares insert queries for the biocyc companion tables in the resistome database.
    
    :param table: 
    :param schema: 
    :param columns: 
    :param arguments: 
    :return: 
    """

    if arguments is None:
        arguments = columns

    fields = '(' + ', '.join(list(columns)) + ')'

    SQL = 'INSERT INTO ' + schema + '.' + table + ' ' + fields + ' VALUES (' + ', '.join(['%s'] * len(arguments)) + ')'
    SQL = unicode(SQL, "UTF-8", "replace")

    log.debug('Prepared sql query: %s' % SQL)

    return SQL


def prepare_tuple_style_sql_query(table, schema, columns):

    fields = '(' + ', '.join(list(columns)) + ')'

    SQL = 'INSERT INTO ' + schema + '.' + table + ' ' + fields + ' VALUES %s'
    SQL = unicode(SQL, "UTF-8", "replace")

    log.debug('Prepared sql query: %s' % SQL)

    return SQL


def biocyc_object_parser(obj, kv_delimiter, general_entry_delimiter, specific_entry_delimiter):

    """
    
    Parses the text flat record files that Biocyc uses to encode their data.
    
    :param obj: list, lines comprising the object
    :param kv_delimiter: str, denoting the separator between key/value
    :param general_entry_delimiter: str, default delimiter for separating keys with multiple values
    :param specific_entry_delimiter: str, override for general_entry_delimiter
    :return: dict (str : str) parsed object
    """

    obj_dict = {}
    previous_key = ''

    index = 0

    log.info('Parsing file into list of dicts (biocyc_object_parser)')

    for line in obj:

        # this leaves any empty entries intact
        line = line.translate(None, '\n\r')

        # removes html tags from input database
        line = re.sub('<.*?>', '', line)

        # continuation from previous key ('/' means line break in the file)
        if '/' == line[0]:

            try:
                obj_dict[previous_key] = obj_dict[previous_key] + \
                                                general_entry_delimiter + line.translate(None, '/')
            except:
                log.error('Error processing multi-line comment: %s (biocyc_object_parser)' % line)
                raise AssertionError('Error processing multi-line comment: %s' % line)

        else:
            # split only at first hyphen
            tokens = line.split(kv_delimiter, 1)

            try:
                # originally I was using biocyc field names as columns, postgres doesn't allow hyphens in column names.
                # you can remove this replace statement if desired.
                key = tokens[0].strip().replace("-", "_")
                value = tokens[1].strip()
            except:
                log.error('Error processing multi-line comment: %s (biocyc_object_parser)' % line)
                raise AssertionError('Failed to process single line comment: %s' % line)

            # multiple entries for same key
            if key in obj_dict:
                old_value = obj_dict[key]

                if key in specific_entry_delimiter:
                    new_value = old_value + specific_entry_delimiter[key] + value
                else:
                    new_value = old_value + general_entry_delimiter + value

                obj_dict[key] = new_value
            else:
                obj_dict[key] = value

            # reaction database has coefficients stored oddly
            # will result in a list of coefficients matching the react order
            if key == 'LEFT' or key == 'RIGHT':
                # still more reactants/products to go
                if index + 1 < len(obj) and obj[index + 1].find('^COEFFICIENT') == -1:
                    obj.insert(index + 1, '^COEFFICIENT - 1')
                # last entry in reaction database
                if index + 1 >= len(obj):
                    obj.append('^COEFFICIENT - 1')

            previous_key = key

        index = index + 1

    log.info('Finished parsing list into list of dicts (biocyc_object_parser)')

    return obj_dict


def parse_fasta(filename):

    """
    
    Simple fasta parser.
    
    :param filename: 
    :return: 
    """

    fasta_dict = defaultdict(list)

    fasta_naive_dict = DatabaseUtils.load_fasta(filename)

    log.debug('Loaded %s for fasta parsing (parse_fasta)' % filename)

    for key in fasta_naive_dict:

        if '|' in key:
            unique_id = key.strip().split('|')[2].split(' ')[0]
        else:
            unique_id = key.strip().split(' ')[0][1:]

        fasta_dict[unique_id] = fasta_naive_dict[key]

    return fasta_dict


def process_biocyc_file(file_name):

    """
    
    Handles the parsing of biocyc flat files into list of lists that are subsequently parsed into dicts.
    
    :param file_name: 
    :return: 
    """

    fhandle = open(file_name, 'r')
    lines = fhandle.readlines()
    fhandle.close()

    log.debug('Loaded %s biocyc file for parsing (process_biocyc_file)' % file_name)

    biocyc_objects = []
    object_wrapper = []

    for line in lines:

        # ignore comments in biocyc flat files
        if line[0] != '#' and len(line) > 0:

            # record element, add to objectWrapper array so long as we haven't found the record separator
            if line[0:2] != "//":
                object_wrapper.append(line)

            # reached record delimiter
            else:
                biocyc_objects.append(object_wrapper)
                object_wrapper = []

    if len(object_wrapper) > 0:
        biocyc_objects.append(object_wrapper)

    return biocyc_objects


def extract_uniprot_xrefs(xref_text):

    """
    
    Extracts Uniprot cross references from dblinks field in biocyc entries and returns them as a list.
    
    :param xref_text: 
    :return: 
    """

    if 'UNIPROT' not in xref_text:

        return []

    # remove parantheses
    xref_text = xref_text.replace('(', '').replace(')', '')

    xrefs = xref_text.split(',')

    temp_output = []

    for token in xrefs:
        components = token.split(' ')
        database = components[0]
        accession = components[1].strip('"')
        temp_output.append((database, accession))

    # remove any non-uniprot cross-links
    temp_output = filter(lambda x: x[0] == 'UNIPROT', temp_output)

    return temp_output


def extract_regulator_data(regulatory_objects):

    """
    
    Extracts gene and other element transcriptional regulation from biocyc.
    
    :param regulatory_objects: 
    :return: 
    """

    output = []

    for regulatory_obj in regulatory_objects:

        mode = regulatory_obj.get('MODE', 'U')
        regulated_entity = regulatory_obj['REGULATED_ENTITY']
        regulator = regulatory_obj['REGULATOR']

        output.append((mode, regulated_entity, regulator))

    return output


def build_protein_disambiguator(protein_objects):

    """
    
    Protein objects are annotated in many different ways in Biocyc databases; some as the monomeric proteins,
    some as protein complexes, some as proteins with post-translational modifications, etc. This method attempts
    to convert an arbitrary protein id back to its monomeric equivalent(s).
    
    :param protein_objects: list of protein IDs
    :return: list of monomeric protein ids
    """

    name_converter = defaultdict(list)
    complexes = []

    monomeric_proteins = set()

    for unique_id in protein_objects:

        if 'UNMODIFIED_FORM' in protein_objects[unique_id] and 'Complexes' not in protein_objects[unique_id]['TYPES']:
            if 'CPLX' in protein_objects[unique_id]['UNMODIFIED_FORM']:
                complexes.append((unique_id, unique_id))
            else:
                name_converter[unique_id].append(protein_objects[unique_id]['UNMODIFIED_FORM'])
        elif 'Complexes' in protein_objects[unique_id]['TYPES'] or 'COMPONENTS' in protein_objects[unique_id]:
            complexes.append((unique_id, unique_id))
        else:
            name_converter[unique_id].append(unique_id)

    monomeric_proteins.update(name_converter.keys())

    seen = set()

    # this is like a low rent stack
    for (original_id, unique_id) in complexes:

        if unique_id not in protein_objects:
            continue

        if (original_id, unique_id) in seen:
            # this happens due to some weird complex annotations (self-referential complexes with no components?)
            continue

        seen.add((original_id, unique_id))
        components = set(protein_objects[unique_id].get('COMPONENTS', unique_id).split(' '))

        if 'UNMODIFIED_FORM' in protein_objects[unique_id]:
            components.add(protein_objects[unique_id]['UNMODIFIED_FORM'])

        monomeric_components = set(filter(lambda x: x in monomeric_proteins, components))

        complex_components = components - monomeric_components
        complex_components = [(original_id, x) for x in complex_components]

        complexes.extend(complex_components)

        for name in monomeric_components:
            name_converter[original_id].append(name)

    return name_converter


def parse_biocyc_flatentry(obj, file_type, kv_delimiter, entry_delimiter):

    """
    
    Wrapper for calling biocyc_object_parser. Automatically specifies some additional delimiters to better
    handle specific fields (see below for examples).
    
    :param obj: list of text representing a record
    :param file_type: str, name of the file the data is extracted from {'reactions', 'compounds'} have special handling
    :param kv_delimiter: str, key value delimiter
    :param entry_delimiter: str, delimiter for keys with multiple values
    :return: dict(str:str), parsed object
    """

    delim_map = {}

    if file_type in {'reactions'}:
        delim_map['^COEFFICIENT'] = ','
        delim_map['LEFT'] = '+'
        delim_map['RIGHT'] = '+'
        delim_map['IN_PATHWAY'] = ','
        delim_map['ENZYMATIC_REACTION'] = ','
    elif file_type in {'compounds'}:
        delim_map['CHEMICAL_FORMULA'] = ','
        delim_map['SYNONYMS'] = ','
        delim_map['REGULATES'] = ','
        delim_map['COFACTORS_OF'] = ','
    else:
        delim_map['SYNONYMS'] = ','
        delim_map['DBLINKS'] = ','
        delim_map['GO_TERMS '] = ','

    obj_dict = biocyc_object_parser(obj, kv_delimiter, entry_delimiter,
                                    delim_map)

    return obj_dict


def biocyc_name_to_gene_feature(object_dict, feature_name):

    """
    
    Attempts to convert the provided feature name into the associated genes, at the cost of checking the entire
    object dict christmas tree hierarchy.
    
    Searches: proteins, genes, promoters, terminators, and transcriptional units.
    
    :param object_dict: dict (str : dict (str : str)) representing all the data from a biocyc file
    :param feature_name: str, unique id of the feature in question
    :return: list of gene names (if found, otherwise empty list)
    """

    genes_found = []
    transcription_units_to_convert = []

    if feature_name in object_dict['proteins']:
        genes_found.append(object_dict['proteins'][feature_name]['GENE'])

    if feature_name in object_dict['genes']:
        genes_found.append(feature_name)

    if feature_name in object_dict['promoters']:

        if 'COMPONENT_OF' in object_dict['promoters'][feature_name]:
            transcription_units_to_convert.extend(object_dict['promoters'][feature_name]['COMPONENT_OF'].split(' '))

        # the remainder seem to be computationally inferred but unverified promoters that I will ignore.
        # especially since no location is given.

    if feature_name in object_dict['terminators']:

        if 'COMPONENT_OF' in object_dict['terminators'][feature_name]:
            transcription_units_to_convert.extend(object_dict['terminators'][feature_name]['COMPONENT_OF'].split(' '))

    if feature_name in object_dict['tu']:
        transcription_units_to_convert.append(feature_name)

    for tu in transcription_units_to_convert:

        if tu in object_dict['tu']:
            components = object_dict['tu'][tu]['COMPONENTS'].split(' ')

            # filter out non-gene elements
            genes = [x for x in components if x in object_dict['genes']]
            genes_found.extend(genes)

    # convert genes to accessions (preferred text id in database)

    converted_gene_names = []

    for gene in genes_found:

        if gene in object_dict['genes'] and 'ACCESSION_1' in object_dict['genes'][gene]:
            converted_gene_names.append(object_dict['genes'][gene].get('ACCESSION_1', gene))

    return converted_gene_names


def parse_uniprot(file_path):

    """
    
    Extract annotations from an uniprot style file downloaded from their website.
    
    :param file_path: 
    :return: 
    """

    output = defaultdict(list)
    with open(file_path, 'rU') as fhandle:
        for line in fhandle:
            if line[0] == '#':
                continue

            tokens = line.strip().split('\t')
            uniprot_id = tokens[0]
            entry_type = tokens[2]
            start = tokens[3]
            stop = tokens[4]

            info = tokens[-1].split(';')
            info = filter(lambda x: 'Note=' in x, info)

            if len(info) == 0:
                note = None
            else:
                note = info[0].split('=')[1].strip()
            output[uniprot_id].append((entry_type, start, stop, note))
        return output


def parse_ecolinet(filename):

    """
    
    Parses the co-functional relation interaction network developed by Kim et al. (10.1093/database/bav001).
    
    :param filename: 
    :return: 
    """

    output = set()

    with open(filename, 'rU') as f:
        for line in f:
            tokens = line.strip().split('\t')
            geneA = tokens[0].upper()
            geneB = tokens[1].upper()
            direction = '?'

            output.add(('protein-protein', geneA, geneB, direction, 'ecolinet_v1'))

    return output


def parse_regulondb(filename):


    """
    
    Parses the RegulonDB database: http://regulondb.ccg.unam.mx/ from text files obtained in 2015.
    
    :param filename: 
    :return: 
    """

    output = set()

    with open(filename, 'rU') as f:
        for line in f:
            tokens = line.strip().split('\t')
            geneA = tokens[0]
            geneB = tokens[1]
            direction = tokens[2]
            output.add(('protein-genetic', geneA, geneB, direction, 'regulondb'))

    return output


def parse_snap2_file(accession, filename, score_threshold=0.0):

    """
    
    Parses SNAP2 protein mutation effect prediction files. These data are essentially matrices with the
    sequence as rows and a fixed order of AAs as columns, and then a score is assigned using a neural network
    based on mutational training data. Not used currently due to the size of the datasets, but may be useful
    in the future.
    
    See here for details about SNAP2: https://rostlab.org/owiki/index.php/Snap2
    
    :param accession
    :param filename:
    :param score_threshold
    :return: 
    """

    output_tuples = []

    with open(filename, 'rU') as f:

        for line in f:

            if '=>' not in line:
                continue

            tokens = line.strip().split('=>')

            aa_pos = tokens[0].strip()

            score = tokens[1].split('sum =')[1].strip()

            wt_aa = aa_pos[0]
            position = str(int(aa_pos[1:-1])-1)
            candidate_replacement_aa = aa_pos[-1]

            factor = -1 if 'Neutral' in tokens[1] else 1

            score = float(score) / 100.0 * factor

            if abs(score) < score_threshold:
                continue
            else:
                output_tuples.append((accession, position, wt_aa, candidate_replacement_aa, score, 'SNAP2'))

    return output_tuples


def parse_essential_genes_file(filename):

    """
    
    Parses list of essential genes.
    
    :param filename: 
    :return: 
    """

    essential_set = set()

    with open(filename, 'rU') as f:

        f.readline()
        for line in f:
            tokens = line.strip().split()
            gene_name = tokens[0].upper()
            essential_set.add(gene_name)

    return essential_set


def build_schema_dicts(schema_file_dict):

    """
    
    Handles the loading of the required biocyc files to build a species database.
    
    :param schema_file_dict: dict (str : str), file name to file path converter
    :return: 
    """

    entry_delimiter = ' '
    kv_delimiter = ' - '

    schema_dict = defaultdict(dict)

    for schema in schema_file_dict:

        files = schema_file_dict[schema]

        for (file_name, data_type) in files:

            if '.dat' in file_name:

                try:
                    biocyc_objects = process_biocyc_file(file_name)
                    biocyc_dict = dict()
                    for obj in biocyc_objects:
                        obj_dict = parse_biocyc_flatentry(obj, data_type, kv_delimiter, entry_delimiter)
                        biocyc_dict[obj_dict['UNIQUE_ID']] = obj_dict
                except IOError:
                    biocyc_dict = dict()

                schema_dict[schema][data_type] = biocyc_dict

            elif data_type in {'aa', 'dna', 'genome'}:

                schema_dict[schema][data_type] = parse_fasta(file_name)

            elif data_type in {'uniprot'}:

                try:
                    uniprot_dict = parse_uniprot(file_name)
                except IOError:
                    uniprot_dict = dict()

                schema_dict[schema][data_type] = uniprot_dict

            elif data_type in {'ecolinet_interactions'}:

                schema_dict[schema][data_type] = parse_ecolinet(file_name)

            elif data_type in {'regulondb_interactions'}:

                schema_dict[schema][data_type] = parse_regulondb(file_name)

    return schema_dict


def build_mutational_prediction_table(cur, unique_id_to_accession_dict, methods={'inps'}, snap2_genes_to_process=set()):

    """
    
    Loads data predicting the effect of mutations on proteins. Currently only loads INPS data by default.
    
    INPS data obtained from here: 10.1093/bioinformatics/btv291
    
    :param cur
    :param unique_id_to_accession_dict
    :param methods
    :return: 
    """

    columns = ['accession', 'position', 'wt_aa', 'mutant_aa', 'score', 'method']

    if 'inps' in methods:

        inps_protein_data = os.path.join(database_constants.INPUT_DIR, 'protein_predictions', 'inps.pred.txt')

        with open(inps_protein_data, 'rU') as f:

            for line in f:
                tokens = line.strip().split('\t')

                gene = tokens[0]
                strain = tokens[1]
                aa_change = tokens[2]

                wt_aa = aa_change[0]
                mut_aa = aa_change[-1]
                position = int(aa_change[1:-1]) - 1
                score = tokens[3]

                sql = prepare_sql_query('protein_stability', strain, columns)
                cur.execute(sql, (gene, position, wt_aa, mut_aa, score, 'INPS'))

    if 'snap2' in methods:

        if len(snap2_genes_to_process) == 0:
            raise AssertionError('Given the size of the SNAP2 dataset, you need to specify which genes you'
                                 'want to extract data from.')

        import psycopg2.extras

        snap2_dir = os.path.join(database_constants.INPUT_DIR, 'protein_predictions')

        strains_present = [x.upper() for x in database_constants.SPECIES_LIST]

        for strain in strains_present:

            target_directory = os.path.join(snap2_dir, strain)

            files = os.listdir(target_directory)

            files = filter(lambda x: 'snap2' not in x and 'fasta' in x, files)

            sql = prepare_tuple_style_sql_query('protein_stability', strain, columns)

            print 'starting insert into schema %s' % strain

            for fname in files:

                sequence_id = fname
                snap2_data = fname + '.snap2'

                fasta = parse_fasta(os.path.join(target_directory, sequence_id))

                try:
                    accession = unique_id_to_accession_dict[fasta.keys()[0]][0]

                    if accession not in snap2_genes_to_process:
                        continue

                    snap2_tuples = parse_snap2_file(accession,
                                                    os.path.join(target_directory, snap2_data),
                                                    score_threshold=0.0)

                    psycopg2.extras.execute_values(cur,
                                                   sql,
                                                   snap2_tuples,
                                                   page_size=2000)
                except:
                    print 'Error when processing %s' % sequence_id
                    continue

            cur.execute('create index pstable_acc_idx on <species>.protein_stability(accession)'.replace('<species>',
                                                                                                         strain))

            print 'done with schema %s' % strain


def build_sauer_metabolite_table(cur, directory_path):

    """
    
    Loads Sauer gene deletion-metabolite data files from supplementary data. Reference: 10.15252/msb.20167150
    
    :param cur: 
    :return: 
    """

    # z-score minimum for inclusion in the table
    z_score_threshold = 2.765

    unified_std = DatabaseUtils.get_unified_standard()

    gene_annotations = []
    # order of data
    with open(os.path.join(directory_path, 'sample_id_modzscore.txt'), 'rU') as f:
        for line in f:
            gene_annotations.append(line.strip())

    neg_ion_annotations = []
    pos_ion_annotations = []

    sql = prepare_sql_query('ms_ions', 'public', ['ion_id', 'kegg_id', 'name'], ['%s', '%s', '%s'])

    # metabolite information
    with open(os.path.join(directory_path, 'annotation_pos.txt'), 'rU') as f:

        for line in f:
            tokens = line.strip().split('\t')
            ion_mass = tokens[0]
            if len(tokens) > 2:
                name = tokens[1]
                kegg_id = tokens[2]
            else:
                name = None
                kegg_id = None
            pos_ion_annotations.append((ion_mass, kegg_id, name))
            cur.execute(sql, (ion_mass, kegg_id, name))

    with open(os.path.join(directory_path, 'annotation_neg.txt'), 'rU') as f:

        for line in f:
            tokens = line.strip().split('\t')
            ion_mass = tokens[0]
            if len(tokens) > 2:
                name = tokens[1]
                kegg_id = tokens[2]
            else:
                name = None
                kegg_id = None
            neg_ion_annotations.append((ion_mass, kegg_id, name))
            cur.execute(sql, (ion_mass, kegg_id, name))

    neg_matrix = []
    pos_matrix = []

    with open(os.path.join(directory_path, 'modzscore_neg.tsv'), 'rU') as f:
        for line in f:
            metabolite_zscores = map(float, line.strip().split('\t'))
            neg_matrix.append(metabolite_zscores)

    with open(os.path.join(directory_path, 'modzscore_pos.tsv'), 'rU') as f:
        for line in f:
            metabolite_zscores = map(float, line.strip().split('\t'))
            pos_matrix.append(metabolite_zscores)

    pos_affected_metabolites = defaultdict(list)
    neg_affected_metabolites = defaultdict(list)

    for j in range(0, len(gene_annotations)):
        for i in range(0, len(neg_ion_annotations)):
            if abs(neg_matrix[i][j]) > z_score_threshold:
                neg_affected_metabolites[gene_annotations[j]].append((i, neg_matrix[i][j]))

    for j in range(0, len(gene_annotations)):
        for i in range(0, len(pos_ion_annotations)):
            if abs(pos_matrix[i][j]) > z_score_threshold:
                pos_affected_metabolites[gene_annotations[j]].append((i, pos_matrix[i][j]))

    for gene in pos_affected_metabolites:

        # this ignores pseudogenes
        converted_name = unified_std.convert(gene, 'Escherichia coli', strain='MG1655')
        if converted_name == gene.upper():
            continue
        for (m, zscore) in pos_affected_metabolites[gene]:
            (ion_mass, kegg, name) = pos_ion_annotations[m]
            sql = prepare_sql_query('metabolomics', 'mg1655', ['accession', 'metabolite'], ['%s', '%s'])
            cur.execute(sql, (converted_name, ion_mass))

    for gene in neg_affected_metabolites:
        converted_name = unified_std.convert(gene, 'Escherichia coli', strain='MG1655')
        if converted_name == gene.upper():
            continue
        for (m, zscore) in neg_affected_metabolites[gene]:
            (ion_mass, kegg, name) = neg_ion_annotations[m]
            sql = prepare_sql_query('metabolomics', 'mg1655', ['accession', 'metabolite'], ['%s', '%s'])
            cur.execute(sql, (converted_name, ion_mass))


def construct_schema_path_dict(source_dirs):

    """
    
    Sets up a dict describing where all the critical files are for processing a species database.
    
    :param source_dirs: 
    :return: 
    """

    schema_files = defaultdict(list)

    for (source, schema) in source_dirs:

        schema_files[schema].append((os.path.join(source, gene_data), 'genes'))
        schema_files[schema].append((os.path.join(source, protein_data), 'proteins'))
        schema_files[schema].append((os.path.join(source, rna_data), 'rna'))
        schema_files[schema].append((os.path.join(source, dna_sequence), 'dna'))
        schema_files[schema].append((os.path.join(source, protein_sequence), 'aa'))
        schema_files[schema].append((os.path.join(source, genome), 'genome'))
        schema_files[schema].append((os.path.join(source, uniprot), 'uniprot'))
        schema_files[schema].append((os.path.join(source, classes), 'classes'))
        schema_files[schema].append((os.path.join(source, transcriptional_units), 'tu'))
        schema_files[schema].append((os.path.join(source, dna_binding_sites), 'dna_binding_sites'))
        schema_files[schema].append((os.path.join(source, promoters), 'promoters'))
        schema_files[schema].append((os.path.join(source, terminators), 'terminators'))
        schema_files[schema].append((os.path.join(source, regulation), 'regulation'))

        if schema == 'mg1655':
            schema_files[schema].append((os.path.join(database_constants.INPUT_DIR, 'interactions', regulon_db),
                                         'regulondb_interactions'))
            schema_files[schema].append((os.path.join(database_constants.INPUT_DIR, 'interactions', ecolinet),
                                         'ecolinet_interactions'))

    return schema_files


def insert_genetic_code(cur, columns, filename):

    """
    
    Loads E. coli genetic code into table.
    
    :param cur: 
    :param columns: 
    :param filename: 
    :return: 
    """

    with open(filename, 'rU') as fhandle:

        fhandle.readline()
        for line in fhandle:
            tokens = line.strip().split('\t')
            codon = tokens[0]
            aa = tokens[1]
            frequency = tokens[2]

            sql = prepare_sql_query('genetic_code', 'public', columns,
                                    ['%s'] * len(columns))
            cur.execute(sql, (codon, aa, frequency))


def insert_genomic_features(cursor, schema, schema_entries, schema_objects, protein_name_converter):

    """
    
    Extracts the following genomic features:
    
    dnabindsites (dna binding sites, can get regulated genes from regulation.dat/transunits.dat
    promoters
    terminators
    
    Schema (per species)
    
    primary key
    unique id
    name
    feature type (terminator, promoter, binding site)
    start
    stop
    
    linked regulated by table (primary key, unique id, regulator)
    linked controlled genes (primary key, unique id, regulated gene)
    
    :param cursor: 
    :param schema: 
    :param schema_entries: 
    :param schema_objects: 
    :param protein_name_converter: 
    :return: 
    """

    objects_to_insert = []

    for unique_id in schema_objects['dna_binding_sites']:

        object_dict = schema_objects['dna_binding_sites'][unique_id]
        bs_type = 'dbs'
        regulatory_references = object_dict['INVOLVED_IN_REGULATION'].split(' ')

        if 'LEFT_END_POSITION' in object_dict:
            start = int(object_dict['LEFT_END_POSITION'])
            stop = int(object_dict['RIGHT_END_POSITION'])
        elif 'ABS_CENTER_POS' in object_dict:
            try:
                center_position = float(object_dict['ABS_CENTER_POS'])
                site_length = float(object_dict.get('SITE_LENGTH', 1))
                start = int(math.floor(center_position - site_length / 2.0))
                stop = int(math.ceil(center_position - site_length / 2.0))
            except ValueError:
                # unknown position or position not given?
                start = -1
                stop = -1
        else:
            # if no known location is given
            start = -1
            stop = -1

        regulated_info = extract_regulator_data([schema_objects['regulation'][x] for x in regulatory_references])

        for (mode, regulated_entities, regulator) in regulated_info:

            converted_regulated_names = protein_name_converter.get(regulated_entities, [regulated_entities])
            converted_regulator_names = protein_name_converter.get(regulator, [regulator])

            regulators = []
            regulated = []

            for regulated_entity in converted_regulated_names:
                regulated.extend(biocyc_name_to_gene_feature(schema_objects, regulated_entity))

            for regulator_entity in converted_regulator_names:
                regulators.extend(biocyc_name_to_gene_feature(schema_objects, regulator_entity))

            objects_to_insert.append((unique_id, bs_type, start, stop, regulated, regulators))

    for unique_id in schema_objects['promoters']:

        object_dict = schema_objects['promoters'][unique_id]
        site_type = 'promoter'
        if 'REGULATED_BY' in object_dict:
            regulatory_references = object_dict['REGULATED_BY'].split(' ')
        else:
            regulatory_references = []

        # transcriptional start site
        start = int(object_dict.get('ABSOLUTE_PLUS_1_POS', -1))
        stop = start + 1

        regulated_info = extract_regulator_data([schema_objects['regulation'][x] for x in regulatory_references])

        for (mode, regulated_entities, regulator) in regulated_info:

            converted_regulated_names = protein_name_converter.get(regulated_entities, [regulated_entities])
            converted_regulator_names = protein_name_converter.get(regulator, [regulator])

            regulators = []
            regulated = []

            for regulated_entity in converted_regulated_names:
                regulated.extend(biocyc_name_to_gene_feature(schema_objects, regulated_entity))

            for regulator_entity in converted_regulator_names:
                regulators.extend(biocyc_name_to_gene_feature(schema_objects, regulator_entity))

            objects_to_insert.append((unique_id, site_type, start, stop, regulated, regulators))

    for unique_id in schema_objects['terminators']:

        object_dict = schema_objects['terminators'][unique_id]
        site_type = 'terminator'
        start = int(object_dict['LEFT_END_POSITION'])
        stop = int(object_dict['RIGHT_END_POSITION'])

        if 'Rho-Dependent-Terminators' in object_dict['TYPES']:
            # rho accession in mg1655
            regulator = ['B3783']
        else:
            regulator = []

        components = [x for x in object_dict['COMPONENT_OF'].split(' ') if 'TU' in x]

        gene_names = []

        for component in components:
            gene_names.extend(biocyc_name_to_gene_feature(schema_objects, component))

        objects_to_insert.append((unique_id, site_type, start, stop, gene_names, regulator))

    already_inserted = dict()

    for (uid, site_type, start, stop, regulated_genes, regulators) in objects_to_insert:

        if uid not in already_inserted:

            sql = prepare_sql_query('genomic_features',
                                    schema,
                                    schema_entries['genomic_features']) + ' RETURNING feature_id'

            cursor.execute(sql, (uid, site_type, start, stop))
            already_inserted[uid] = cursor.fetchone()['feature_id']

        feature_id = already_inserted[uid]

        sql = prepare_sql_query('feature_regulators',
                                schema,
                                schema_entries['feature_regulators'])

        for regulator in regulators:
            cursor.execute(sql, (feature_id, regulator.upper()))

        sql = prepare_sql_query('feature_regulated_genes',
                                schema,
                                schema_entries['feature_regulated_genes'])

        for regulated_gene in regulated_genes:
            cursor.execute(sql, (feature_id, regulated_gene.upper()))


def insert_genome_sequences(cur, schema, columns, genome_object):

    for key in genome_object:
        gen_seq = genome_object[key]
        sql = prepare_sql_query('genome', schema, columns, ['%s', '%s'])
        cur.execute(sql, (key, gen_seq))


def insert_sequence_data(cur, schema, columns, sequence_table, accession, sequence):

    assert sequence_table == 'aa_sequences' or sequence_table == 'nt_sequences'

    sql = prepare_sql_query(sequence_table, schema, columns, ['%s', '%s'])
    cur.execute(sql, (accession, sequence))


def insert_public_go_information(cur, schema, strain_objects, root_nodes, seen_go_codes):

    go_terms_to_include = set()

    for class_key in strain_objects[schema]['classes']:

        if 'GO:' not in class_key or class_key in seen_go_codes:
            continue

        seen_go_codes.add(class_key)

        types = strain_objects[schema]['classes'][class_key]['TYPES'].split(' ')

        if class_key in root_nodes:
            types = []

        name = strain_objects[schema]['classes'][class_key]['COMMON_NAME']
        sql = prepare_sql_query('go_table', 'public', entries['public_go'])
        cur.execute(sql, (class_key, types, name))

        if 'Gene-Ontology-Terms' not in types:
            go_terms_to_include.add(class_key)

        for ancestor in types:
            if 'GO:' not in ancestor:
                continue
            sql = prepare_sql_query('go_adj_list', 'public', entries['go_adj_list'])
            cur.execute(sql, (ancestor, class_key))
            go_terms_to_include.add(ancestor)

    return go_terms_to_include, seen_go_codes


def insert_uniprot_data(cur, schema, columns, uniprot_dict, uniprot_to_accession):

    for uniprot_id in uniprot_to_accession:

        if uniprot_id not in uniprot_dict:
            continue

        accession = uniprot_to_accession[uniprot_id]

        for (region, start, stop, note) in uniprot_dict[uniprot_id]:

            sql = prepare_sql_query('uniprot', schema, columns, ['%s'] * len(columns))

            try:
                cur.execute(sql, (accession, region, start - 1 if isinstance(start, int) else start, stop, note))
            except:
                raise


def insert_interaction_data(cur, schema, table, columns, data_tuples):

    for data_tuple in data_tuples:
        sql = prepare_sql_query(table,
                                schema,
                                columns,
                                ['%s'] * len(entries['interactions']))
        cur.execute(sql, data_tuple)


def main():

    """
    
    Driver method for database construction; does the heavy lifting of getting the data sources together and 
    then drives the inserts.
    
    :return: 
    """

    # manually get cursor to avoid depending on Database utils for this
    # also, you need to run the biocyc_data_parser to build the biocyc tables first

    try:
        connect = psycopg2.connect("dbname='%s' user='%s' host='localhost' password='%s'" % (database_constants.DB_NAME,
                                                                                             database_constants.DB_USERNAME,
                                                                                             database_constants.DB_PASSWORD))
    except:
        raise

    connect.set_session(autocommit=False)
    cur = connect.cursor(cursor_factory=psycopg2.extras.RealDictCursor)

    root_dir = database_constants.INPUT_DIR
    ECOLI_DIR = os.path.join(root_dir, 'biocyc', 'mg1655', 'data')
    ECOLIB_DIR = os.path.join(root_dir, 'biocyc', 'rel606', 'data')
    ECOLIW_DIR = os.path.join(root_dir, 'biocyc', 'ecoliw', 'data')

    source_dirs = [(ECOLI_DIR, 'mg1655'),
                   (ECOLIB_DIR, 'rel606'),
                   (ECOLIW_DIR, 'w')]

    schema_files = construct_schema_path_dict(source_dirs)

    converter = {}
    # name in column names (below) converted to the biocyc equivalent
    converter['COEFFICIENT'] = '^COEFFICIENT'
    converter['RIGHT_HS'] = 'RIGHT'
    converter['LEFT_HS'] = 'LEFT'
    converter['ORPHAN'] = 'ORPHAN?'

    strain_objects = build_schema_dicts(schema_files)

    # go data
    seen_go_codes = set()
    root_nodes = {'GO:0008150', 'GO:0005575', 'GO:0003674'}
    go_terms_to_include = set()

    # generates schema
    cur.execute('DROP SCHEMA IF EXISTS public CASCADE')
    with open(os.path.join(database_constants.INPUT_DIR, 'sql', 'resistome_biocyc_public.sql'), 'rU') as f:
        sql_schema = ''.join(f.readlines())
        cur.execute(sql_schema)

    unique_id_to_accession = dict()

    # adds genetic code to public table
    insert_genetic_code(cur,
                        entries['genetic_code'],
                        os.path.join(database_constants.INPUT_DIR,
                                     'biological_info',
                                     'Genetic Code EC.txt'))

    # load essential genes
    essential_set = parse_essential_genes_file(os.path.join(database_constants.INPUT_DIR,
                                                            'biological_info',
                                                            'essential_genes_ecoli.txt'))

    for schema in strain_objects:

        cur.execute('DROP SCHEMA IF EXISTS ' + schema + ' CASCADE')

        with open(os.path.join(database_constants.INPUT_DIR, 'sql', 'resistome_biocyc_schema.sql'), 'rU') as f:
            sql_schema = ''.join(f.readlines())
            # make schema species specific
            cur.execute(sql_schema.replace('<species>', schema))

        go_terms_extracted, seen_updated = insert_public_go_information(cur,
                                                                        schema,
                                                                        strain_objects,
                                                                        root_nodes,
                                                                        seen_go_codes)
        go_terms_to_include.update(go_terms_extracted)
        seen_go_codes.update(seen_updated)

        uniprot_to_accession = dict()
        gene_objects = strain_objects[schema]['genes'].values()

        protein_name_converter = build_protein_disambiguator(strain_objects[schema]['proteins'])

        for obj_dict in gene_objects:

            accession = obj_dict.get('ACCESSION_1', None)

            if accession is None:
                continue

            accession = accession.upper()

            unique_gene_id = obj_dict['UNIQUE_ID']
            start = obj_dict.get('LEFT_END_POSITION', None)
            stop = obj_dict.get('RIGHT_END_POSITION', None)
            direction = obj_dict.get('TRANSCRIPTION_DIRECTION', None)
            unique_id_to_accession[unique_gene_id] = (accession, start, stop, direction)

            common_name = obj_dict.get('COMMON_NAME', unique_gene_id)
            essential = common_name.upper() in essential_set

            synonyms = []

            if 'SYNONYMS' in obj_dict:
                synonyms = obj_dict['SYNONYMS'].split(',')

            protein_ids = []

            uniprot_refs = []

            if 'PRODUCT' in obj_dict:
                protein_ids.extend(obj_dict['PRODUCT'].replace('|', '').split(' '))
                for id in protein_ids:
                    unique_id_to_accession[id] = (accession, None, None, None)
                    if id in strain_objects[schema]['proteins']:
                        uniprot_refs.extend(extract_uniprot_xrefs(strain_objects[schema]['proteins'][id].get('DBLINKS', '')))
                    elif id in strain_objects[schema]['rna']:
                        uniprot_refs.extend(extract_uniprot_xrefs(strain_objects[schema]['rna'][id].get('DBLINKS', '')))

            if len(uniprot_refs) > 0:
                for (_, uniprot_id) in uniprot_refs:
                    uniprot_to_accession[uniprot_id] = accession

            go_terms = []

            for protein_id in protein_ids:

                if protein_id in strain_objects[schema]['rna']:
                    table = 'rna'
                    protein_object_dict = strain_objects[schema][table][protein_id]
                    if 'GO_TERMS' in protein_object_dict:
                        go_terms.extend(protein_object_dict['GO_TERMS'].replace('|', '').split(' '))
                elif protein_id in strain_objects[schema]['proteins']:
                    table = 'proteins'
                    protein_object_dict = strain_objects[schema][table][protein_id]
                    if 'GO_TERMS' in protein_object_dict:
                        go_terms.extend(protein_object_dict['GO_TERMS'].replace('|', '').split(' '))

            # start - 1 converts the index to zero

            arguments = [unique_gene_id,
                         common_name,
                         synonyms,
                         protein_ids,
                         accession,
                         start,
                         stop,
                         direction,
                         essential]

            SQL = prepare_sql_query('genes', schema, entries['genes'], arguments)
            try:
                cur.execute(SQL, arguments)
            except:
                print SQL
                print arguments
                raise

            for go_term in go_terms:

                if go_term not in go_terms_to_include:
                    continue

                SQL = prepare_sql_query('go_terms', schema, entries['go_terms'], [accession, go_term])
                cur.execute(SQL, (accession, go_term))

            if unique_gene_id in strain_objects[schema]['dna']:
                insert_sequence_data(cur,
                                     schema,
                                     entries['dna'],
                                     'nt_sequences',
                                     accession,
                                     strain_objects[schema]['dna'][unique_gene_id])

            for protein_id in protein_ids:
                if protein_id in strain_objects[schema]['aa']:
                    insert_sequence_data(cur,
                                         schema,
                                         entries['aa'],
                                         'aa_sequences',
                                         accession,
                                         strain_objects[schema]['aa'][protein_id])

        insert_genome_sequences(cur, schema, entries['genome'], strain_objects[schema]['genome'])

        uniprot_dict = strain_objects[schema]['uniprot']
        insert_uniprot_data(cur, schema, entries['uniprot'], uniprot_dict, uniprot_to_accession)

        insert_interaction_data(cur, schema, 'interactions', entries['interactions'],
                                strain_objects[schema].get('regulondb_interactions', []))
        insert_interaction_data(cur, schema, 'interactions', entries['interactions'],
                                strain_objects[schema].get('ecolinet_interactions', []))

        insert_genomic_features(cur, schema, entries, strain_objects[schema], protein_name_converter)

        print 'Finished parsing provided biocyc database: %s' % schema

    build_sauer_metabolite_table(cur, os.path.join(database_constants.INPUT_DIR, 'metabolomics'))

    # allowable method types include 'inps' and 'snap2'
    # warning: loading the snap2 data takes quite a bit of time...
    build_mutational_prediction_table(cur, unique_id_to_accession,
                                      methods={'inps', 'snap2'},
                                      snap2_genes_to_process={'B3357',
                                                              'B3251',
                                                              'B3988',
                                                              'B3261',
                                                              'B0462',
                                                              'B3987',
                                                              'B3067',
                                                              'B4018',
                                                              'B3783',
                                                              'B4063'})

    connect.commit()

    connect.close()

if __name__ == '__main__':

    # todo: add table of dna binding proteins - consensus sequences

    main()
