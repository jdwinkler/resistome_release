import biocyc_data_parser
import sql_generator
import database_constants
import os

print 'Using the following credentials to create the Resistome database:'
print 'Credential file path: %s' % os.path.join(database_constants.INPUT_DIR, 'db_credentials')
print 'Database name: %s' % database_constants.DB_NAME
print 'Username: %s' % database_constants.DB_USERNAME
print 'Password: %s\n' % database_constants.DB_PASSWORD

print 'Building Biocyc databases'

try:
    biocyc_data_parser.main()
except:
    print 'Exception occurred when generating biocyc supporting databases.'
    print 'Possible causes include missing data (biocyc data is distributed separately from the rest of the database' \
          ' for copyright reasons), the database credentials are wrong, the host database has not been set up, or ' \
          'an unknown cause.'
    raise

print 'Building Resistome tables'
try:
    sql_generator.main(add_resistome_go_metabolite_tables=True)
except:
    print 'Exception occurred when building Resistome database'
    print 'This is most likely caused by the target Postgres database not being set up, or new data failing QC.'
    raise